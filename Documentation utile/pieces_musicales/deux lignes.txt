// Notez bien que la ligne peut être très longue (il n'y a pas de limite)
// L'artiste peut toutefois décider de séparer sa pièce en paragraphes en laissant des lignes vides.
// Chaque paragraphe contient une ligne de note et (optionnellement) une ligne avec les durées.
60

|G G A B | |G G A B |
 
|C C B A| | B B A G |
