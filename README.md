# Gaudrophone
Music application that allows you to create and play musical instruments based on Java MIDI framework. You can also record and replay your own compositions in loop and create your own compositions into a text file that the application will read and play automatically. You can also learn those compositions on your instrument into a `Guitar Hero` like game.

# How to run
The application is already compiled into a fat jar under `livrables/Gaudrophone-fat.jar`.
There are also available compositions written by our team under `Documentation utile/pieces_musicales`.
