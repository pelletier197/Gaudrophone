/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package button;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author sunny
 */
public class ScaledImageButton extends JButton {

    private ImageIcon icon;

    public ScaledImageButton() {
        super();
        init();
    }

    public ScaledImageButton(String text, ImageIcon icon) {
        super(text);
        init();
        setIcon(icon);
    }

    @Override
    public void setIcon(Icon defaultIcon) {
        this.icon = (ImageIcon) defaultIcon;
        resizeIcon(getMinimumSize());
    }

    private void resizeIcon(Dimension size) {

        Insets insets = getInsets();
        size.width -= insets.left + insets.right;
        size.height -= insets.top + insets.bottom;
        if (size.width > size.height) {
            size.width = -1;
        } else {
            size.height = -1;
        }

        size.width = size.width == 0 ? 1 : size.width;

        ImageIcon icon = (ImageIcon) this.icon;
        Image scaled = icon.getImage().getScaledInstance(size.width, size.height, java.awt.Image.SCALE_SMOOTH);
        super.setIcon(new ImageIcon(scaled));
    }

    private void init() {

        addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                resizeIcon(getSize());
            }
        });
    }
}
