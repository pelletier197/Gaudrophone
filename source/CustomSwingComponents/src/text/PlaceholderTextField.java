/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.Document;

/**
 *
 * @author sunny
 */
public class PlaceholderTextField extends JTextField {

    private String placeholder;

    public PlaceholderTextField() {
        this.placeholder = null;
    }

    public PlaceholderTextField(String placeholder) {
        this.placeholder = placeholder;
    }

    public PlaceholderTextField(String placeholder, String text) {
        super(text);
        this.placeholder = placeholder;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    @Override
    protected void paintComponent(final Graphics pG) {
        super.paintComponent(pG);

        if (placeholder == null || placeholder.length() == 0 || getText().length() > 0) {
            return;
        }

        final Graphics2D g = (Graphics2D) pG;
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(getDisabledTextColor());
        float marginTop = getHeight()/2+ pG.getFontMetrics().getMaxAscent()/2 - getInsets().top/2;
        g.drawString(placeholder, getInsets().left, marginTop);
    }

    public void setPlaceholder(final String s) {
        placeholder = s;
    }
}
