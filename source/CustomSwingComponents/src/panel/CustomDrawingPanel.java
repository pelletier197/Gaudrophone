/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 *
 * @author sunny
 */
public class CustomDrawingPanel extends JPanel {
    
    private CustomDrawer customDrawer;
    
    public void setCustomDrawer(CustomDrawer drawer) {
        this.customDrawer = drawer;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if (customDrawer != null) {
            customDrawer.draw((Graphics2D) g);
        }
    }
    
}
