/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.apparence;

import java.io.Serializable;

/**
 *
 * @author sunny
 */
public class Border implements Serializable {

    private static final int MAX_WIDTH = 15;

    private Color color;
    private BorderType type;
    private int width;

    public Border() {
        this.color = new Color(0x00000000);
        this.width = 1;
        this.type = BorderType.SOLID;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public BorderType getType() {
        return type;
    }

    public void setType(BorderType type) {
        this.type = type;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (width >= 0 && width <= MAX_WIDTH) {
            this.width = width;
        }
    }

}
