/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.apparence;

import java.io.Serializable;

/**
 *
 * @author sunny
 */
public class Color implements Serializable {

    private int a, r, g, b;

    /**
     * This constructor makes a default Color.
     */
    public Color() {
        this(255, 255, 255, 255);
    }

    public Color(int argb) {

        final int mask = 0xFF;

        a = (argb >> 24) & mask;
        r = (argb >> 16) & mask;
        g = (argb >> 8) & mask;
        b = (argb) & mask;

    }

    public Color(int r, int g, int b) {
        this(255, r, g, b);
    }

    public Color(int a, int r, int g, int b) {
        setAll(a, r, g, b);
    }

    private boolean validColorRange(int colorValue) {
        return colorValue >= 0 && colorValue <= 255;
    }

    public void setAll(int a, int r, int g, int b) {
        setAlpha(a);
        setRed(r);
        setGreen(g);
        setBlue(b);
    }

    public void setAlpha(int a) {
        if (!validColorRange(a)) {
            throw new IllegalArgumentException("Illegal color value : " + a);
        }
        this.a = a;
    }

    public void setRed(int r) {
        if (!validColorRange(r)) {
            throw new IllegalArgumentException("Illegal color value : " + r);
        }
        this.r = r;
    }

    public void setGreen(int g) {
        if (!validColorRange(g)) {
            throw new IllegalArgumentException("Illegal color value : " + g);
        }
        this.g = g;
    }

    public void setBlue(int b) {
        if (!validColorRange(b)) {
            throw new IllegalArgumentException("Illegal color value : " + b);
        }
        this.b = b;
    }

    public int getAlpha() {
        return a;
    }

    public int getRed() {
        return r;
    }

    public int getGreen() {
        return g;
    }

    public int getBlue() {
        return b;
    }

}
