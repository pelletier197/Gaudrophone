/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.apparence;

import gaudrophone.instrument.Note;
import java.io.Serializable;

/**
 *
 * @author sunny
 */
public enum BorderType implements Serializable{
    SOLID("Pleine"),
    DOTTED("Pointillée");
    
    private final String display;

    private BorderType(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return display;
    }

    public static BorderType fromString(String toString) {
        for (BorderType b : BorderType.values()) {
            if (b.toString().equalsIgnoreCase(toString)) {
                return b;
            }
        }
        return null;
    }

}
