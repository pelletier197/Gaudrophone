/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry;

import java.io.Serializable;

/**
 * Regroups all general methods of shapes to simplify implementation.
 *
 * @author sunny
 */
public abstract class AbstractShape implements Shape, Serializable {

    protected Point center;
    protected double angle;

    public AbstractShape() {
        center = new Point(0, 0);
    }

    @Override
    public void setAngle(double angleDegree) {
        double delta = angleDegree - angle;
        rotate(delta);
    }

    @Override
    public void translate(double deltaX, double deltaY) {
        center.x += deltaX;
        center.y += deltaY;
    }

    @Override
    public void rotate(double degrees) {
        angle += degrees;
    }

    @Override
    public double getAngle() {
        return angle;
    }

    @Override
    public void setPosition(Point centerPosition) {
        double deltaX = centerPosition.x - center.x;
        double deltaY = centerPosition.y - center.y;

        translate(deltaX, deltaY);
    }

    @Override
    public Point getCenter() {
        return center;
    }

    @Override
    public void scale(double scaleFactor) {
        scale(scaleFactor, scaleFactor);
    }

    @Override
    public void setHeight(double height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Can't have a height of 0");
        }
        // Ensures the shape conserves its general look
        double angleBefore = angle;
        setAngle(0);

        double current = getHeight();
        double factor = height / current;

        scale(1, factor);

        // Restore angle
        setAngle(angleBefore);

    }

    @Override
    public void setWidth(double width) {
        // Ensures the shape conserves its general look
        double angleBefore = angle;
        setAngle(0);

        double current = getWidth();
        double factor = width / current;

        scale(factor, 1);

        // Restore angle
        setAngle(angleBefore);

    }
}
