/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry;

import java.io.Serializable;

/**
 *
 * @author sunny
 */
public interface Shape extends Serializable{

    public boolean isInside(Point p);

    public void rotate(double degrees);

    public void setAngle(double degrees);

    public double getAngle();

    public void translate(Point pDeltaPosition);

    public void translate(double deltaX, double deltaY);

    public void setPosition(Point p);

    public Point getCenter();

    public Point[] getPoints();

    public void scale(double scaleFactor);

    public void scale(double scaleX, double scaleY);

    public void setHeight(double height);

    public void setWidth(double width);

    public double getHeight();

    public double getWidth();

}
