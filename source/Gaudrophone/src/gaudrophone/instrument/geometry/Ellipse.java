/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry;

import java.io.Serializable;

/**
 *
 * @author sunny
 */
public class Ellipse extends AbstractShape implements Serializable{

    private double width;
    private double height;

    /**
     * Those data are computed
     */
    private double sinAngle, cosAngle;
    
    /**
     * This constructor makes a basic circle Ellipse.
     */
    public Ellipse() {
        super();
        this.width = 1;
        this.height = 1;
    }

    public Ellipse(double height, double width, Point center) {
        super();
        super.setPosition(center);
        this.width = width;
        this.height = height;

        sinAngle = 0; // sin(0)
        cosAngle = 1; // cos(0)
    }

    public Ellipse(double height, double width, double x, double y) {
        this(height, width, new Point(x, y));
    }

    /**
     * @see
     * https://math.stackexchange.com/questions/426150/what-is-the-general-equation-of-the-ellipse-that-is-not-in-the-origin-and-rotate
     * @param p
     * @return
     */
    @Override
    public boolean isInside(Point p) {
        double x = p.x;
        double y = p.y;
        double h = center.x;
        double k = center.y;

        double a = width / 2;
        double b = height / 2;

        double first = ((x - h) * cosAngle) + ((y - k) * sinAngle);
        double second = ((x - h) * sinAngle) - ((y - k) * cosAngle);

        return ((first * first) / (a * a)) + ((second * second) / (b * b)) <= 1;

    }

    @Override
    public Point[] getPoints() {
        return new Point[]{center};
    }

    @Override
    public void scale(double scaleX, double scaleY) {
        if (scaleX == 0 || scaleY == 0) {
            throw new IllegalArgumentException("Illegal scale factor");
        }

        width *= scaleX;
        height *= scaleY;
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        this.width = width;
    }

    @Override
    public void setHeight(double height) {
        super.setHeight(height);
        this.height = height;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public double getWidth() {
        return width;
    }

    public double getRectangleWidth() {
        // https://math.stackexchange.com/questions/91132/how-to-get-the-limits-of-rotated-ellipse
        double a = width / 2;
        double b = height / 2;

        return 2 * Math.sqrt((a * a * cosAngle * cosAngle) + (b * b * sinAngle * sinAngle));
        // return Math.abs(width * Math.cos(Math.toRadians(getAngle())));
    }

    public double getRectangleHeight() {
        // https://math.stackexchange.com/questions/91132/how-to-get-the-limits-of-rotated-ellipse
        double a = width / 2;
        double b = height / 2;
        return 2 * Math.sqrt((a * a * sinAngle * sinAngle) + (b * b * cosAngle * cosAngle));
        //return Math.abs(width * Math.sin(Math.toRadians(getAngle())));
    }

    @Override
    public void setAngle(double angleDegree) {
        super.setAngle(angleDegree);
        computeAngles();
    }

    @Override
    public void rotate(double angle) {
        super.rotate(angle);
        computeAngles();
    }

    private void computeAngles() {

        double angleRad = Math.toRadians(angle);

        this.sinAngle = Math.sin(angleRad);
        this.cosAngle = Math.cos(angleRad);
    }

    @Override
    public void translate(Point pDeltaPosition) {
        super.translate(pDeltaPosition.x, pDeltaPosition.y);
    }
}
