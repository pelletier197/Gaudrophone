/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry.shapes;

import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;
import java.io.Serializable;

/**
 *
 * @author sunny
 */
public class Hexagon extends Polygon implements Serializable{

    public Hexagon() {
        super(new Point[]{
            new Point(0, 0.5),
            new Point(0.288675134, 0),
            new Point(1 - 0.288675134, 0),
            new Point(1, 0.5),
            new Point(1 - 0.288675134, 1),
            new Point(0.288675134, 1)
        });
    }

}
