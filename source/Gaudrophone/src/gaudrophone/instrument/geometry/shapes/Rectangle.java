/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry.shapes;

import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;
import java.io.Serializable;
import java.util.Arrays;

/**
 *
 * @author sunny
 */
public class Rectangle extends Polygon implements Serializable {

    public Rectangle() {
        super(new Point[]{
            new Point(0, 0),
            new Point(1, 0),
            new Point(1, 1),
            new Point(0, 1)
        });
    }

    public Rectangle(Point[] points) {
        super(points);
        if (!isRectangle(points)) {
            throw new IllegalArgumentException("Not a rectangle");
        }
    }

    private boolean isRectangle(Point[] points) {
        // https://stackoverflow.com/questions/2303278/find-if-4-points-on-a-plane-form-a-rectangle
        if (points.length != 4) {
            return false;
        }
        return isRectangle(points[0], points[1], points[2], points[3]);
    }

    boolean isRectangle(Point a, Point b, Point c, Point d) {
        return isOrthogonal(a, b, c)
                && isOrthogonal(b, c, d)
                && isOrthogonal(c, d, a);
    }

    private boolean isOrthogonal(Point a, Point b, Point c) {
        return (b.getX() - a.getX()) * (b.getX() - c.getX()) + (b.getY() - a.getY()) * (b.getY() - c.getY()) == 0;
    }

}
