/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry.shapes;

import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;
import java.io.Serializable;

/**
 *
 * @author sunny
 */
public class Octogon extends Polygon implements Serializable{

    public Octogon() {
        // https://math.stackexchange.com/questions/925677/can-anyone-give-me-x-y-coordinates-for-an-octagon
        super(new Point[]{
            new Point(1, 0.5),
            new Point(0.85355339, 0.85355339),
            new Point(0.5, 1),
            new Point(0.146446609, 0.85355339),
            new Point(0, 0.5),
            new Point(0.146446609, 0.146446609),
            new Point(0.5, 0),
            new Point(0.85355339, 0.146446609)
        });
    }

}
