/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.search;

import gaudrophone.instrument.InstrumentKey;

/**
 *
 * @author Stéphanie Mercier
 */
public abstract class AbstractInstrumentKeySearchStrategy implements InstrumentKeySearchStrategy {

    protected String expression;    

    @Override
    public void setExpression(String expression) {
        this.expression = expression;
    }
    
}
