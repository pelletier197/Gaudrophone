/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.search;

import gaudrophone.instrument.InstrumentKey;

/**
 *
 * @author sunny
 */
public interface InstrumentKeySearchStrategy {
    
    public boolean filter(InstrumentKey key);

    public void setExpression(String expression);
    
}
