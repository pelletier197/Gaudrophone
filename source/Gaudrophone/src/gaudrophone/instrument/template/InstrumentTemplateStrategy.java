/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.template;

import gaudrophone.instrument.Instrument;

/**
 *
 * @author sunny
 */
public interface InstrumentTemplateStrategy {

    public Instrument generateInstrument();

}
