/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.template;

import gaudrophone.controller.GeneratedInstrumentType;
import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.Note;
import gaudrophone.instrument.apparence.Border;
import gaudrophone.instrument.apparence.Color;
import gaudrophone.instrument.geometry.Ellipse;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Shape;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.sound.generated.GeneratedSoundInstrument;

/**
 *
 * @author sunny
 */
public class GuitarTemplateStrategy implements InstrumentTemplateStrategy {

    // https://takelessons.com/blog/wp-content/uploads/2014/01/guitar-fretboard.jpg
    private static final Note[][] NOTES = new Note[][]{
        new Note[]{Note.F, Note.F_SHARP, Note.G, Note.G_SHARP, Note.A, Note.A_SHARP, Note.B, Note.C, Note.C_SHARP, Note.D, Note.D_SHARP, Note.E},
        new Note[]{Note.C, Note.C_SHARP, Note.D, Note.D_SHARP, Note.E, Note.F, Note.F_SHARP, Note.G, Note.G_SHARP, Note.A, Note.A_SHARP, Note.B},
        new Note[]{Note.G_SHARP, Note.A, Note.A_SHARP, Note.B, Note.C, Note.C_SHARP, Note.D, Note.D_SHARP, Note.E, Note.F, Note.F_SHARP, Note.G},
        new Note[]{Note.D_SHARP, Note.E, Note.F, Note.F_SHARP, Note.G, Note.G_SHARP, Note.A, Note.A_SHARP, Note.B, Note.C, Note.C_SHARP, Note.D},
        new Note[]{Note.A_SHARP, Note.B, Note.C, Note.C_SHARP, Note.D, Note.D_SHARP, Note.E, Note.F, Note.F_SHARP, Note.G, Note.G_SHARP, Note.A},
        new Note[]{Note.F, Note.F_SHARP, Note.G, Note.G_SHARP, Note.A, Note.A_SHARP, Note.B, Note.C, Note.C_SHARP, Note.D, Note.D_SHARP, Note.E}
    };

    private static final double KEY_WIDTH = (1 - 0.03) / 13;
    private static final double KEY_HEIGHT = (0.5 / 6);

    private static final double START_Y = 0.25 + (KEY_HEIGHT / 2);
    private static final double START_X = 0.02 + (KEY_WIDTH / 2);

    private static final double FRET_SEPARATOR_WIDTH = 0.0008;
    private static final double MARKER_DIAMETER = 0.6 * KEY_HEIGHT;

    private static final Color FRET_COLOR = new Color(0xFF835C3B);
    private static final Color FREE_FRET_COLOR = new Color(0xFF472000);
    private static final Color ROPE_COLOR = new Color(0xFF646464);
    private static final Color FRET_SEPARATOR_COLOR = new Color(0xF4404144);
    private static final Color MARKER_COLOR = new Color(0x5fc0c3c6);

    private static final GeneratedSoundInstrument TYPE = GeneratedSoundInstrument.ACOUSTIC_GUITAR;

    @Override
    public Instrument generateInstrument() {
        Instrument instrument = new Instrument();
        instrument.setName(toString());

        drawOctave3(instrument);
        drawOctave4(instrument);
        drawOctave5(instrument);
        drawOctave6(instrument);
        drawFreeFrets(instrument);
        drawFretSeparators(instrument);
        drawCircles(instrument);
        drawStrings(instrument);

        return instrument;
    }

    @Override
    public String toString() {
        return "Guitare";
    }

    private void drawOctave3(Instrument instrument) {
        int i = 4; // 5Ft cord

        for (int j = 0; j < 2; j++) {
            createFret(instrument, NOTES[i][j], 3, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }
        createFret(instrument, NOTES[i][2], 4, START_X + (2 * KEY_WIDTH), START_Y + i * KEY_HEIGHT);

        i = 5;
        for (int j = 0; j < 7; j++) {
            createFret(instrument, NOTES[i][j], 3, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }
        createFret(instrument, NOTES[i][7], 4, START_X + (7 * KEY_WIDTH), START_Y + i * KEY_HEIGHT);

    }

    private void drawOctave4(Instrument instrument) {
        int i = 1;

        createFret(instrument, NOTES[i][0], 5, START_X, START_Y + i * KEY_HEIGHT);

        i = 2;
        for (int j = 0; j < 4; j++) {
            createFret(instrument, NOTES[i][j], 4, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }
        createFret(instrument, NOTES[i][4], 5, START_X + (4 * KEY_WIDTH), START_Y + i * KEY_HEIGHT);

        i = 3;
        for (int j = 0; j < 9; j++) {
            createFret(instrument, NOTES[i][j], 4, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }
        createFret(instrument, NOTES[i][9], 5, START_X + (9 * KEY_WIDTH), START_Y + i * KEY_HEIGHT);

        i = 4;
        for (int j = 3; j < 12; j++) {
            createFret(instrument, NOTES[i][j], 4, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }

        i = 5;
        for (int j = 8; j < 12; j++) {
            createFret(instrument, NOTES[i][j], 4, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }

    }

    private void drawOctave5(Instrument instrument) {
        int i = 0;
        for (int j = 0; j < 7; j++) {
            createFret(instrument, NOTES[i][j], 5, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }
        createFret(instrument, NOTES[i][7], 6, START_X + (7 * KEY_WIDTH), START_Y + i * KEY_HEIGHT);

        i = 1;
        for (int j = 1; j < 12; j++) {
            createFret(instrument, NOTES[i][j], 5, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }

        i = 2;
        for (int j = 5; j < 12; j++) {
            createFret(instrument, NOTES[i][j], 5, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }

        i = 3;
        for (int j = 10; j < 12; j++) {
            createFret(instrument, NOTES[i][j], 5, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }
    }

    private void drawOctave6(Instrument instrument) {
        int i = 0;
        for (int j = 8; j < 12; j++) {
            createFret(instrument, NOTES[i][j], 6, START_X + (j * KEY_WIDTH), START_Y + i * KEY_HEIGHT);
        }
    }

    private void createFret(Instrument instrument, Note n, int octave, double poxX, double posY) {

        InstrumentKey key = instrument.addInstrumentKey();

        key.getShape().setPosition(new Point(poxX, posY));
        key.getShape().setWidth(KEY_WIDTH);
        key.getShape().setHeight(KEY_HEIGHT);
        key.setBackgroundColor(FRET_COLOR);
        key.setSound(new GeneratedSound(n, octave - 1, TYPE));
        key.setBorder(new Border());
        key.getBorder().setWidth(0);
        key.setPersistance(1000);

    }

    private void drawStrings(Instrument instrument) {

        InstrumentKey key = instrument.addInstrumentKey();
        key.getShape().setPosition(new Point(START_X - KEY_WIDTH / 2 - 0.01, 0.5));
        key.getShape().setHeight(0.5);
        key.getShape().setWidth(0.02);
        key.setBackgroundColor(ROPE_COLOR);
        key.setPlayable(false);

        for (int i = 0; i < 6; i++) {
            key = instrument.addInstrumentKey();
            key.getShape().setPosition(new Point(0.5 + 0.01, START_Y + i * KEY_HEIGHT));
            key.getShape().setHeight(KEY_HEIGHT / (18 - (2 * i)));
            key.getShape().setWidth(0.98);
            key.setBackgroundColor(ROPE_COLOR);
            key.setPlayable(false);
        }
    }

    private void drawFreeFrets(Instrument instrument) {
        createFreeFret(instrument, Note.E, 5, START_X + 12 * KEY_WIDTH, START_Y);
        createFreeFret(instrument, Note.B, 4, START_X + 12 * KEY_WIDTH, START_Y + KEY_HEIGHT);
        createFreeFret(instrument, Note.G, 4, START_X + 12 * KEY_WIDTH, START_Y + 2 * KEY_HEIGHT);
        createFreeFret(instrument, Note.D, 4, START_X + 12 * KEY_WIDTH, START_Y + 3 * KEY_HEIGHT);
        createFreeFret(instrument, Note.A, 3, START_X + 12 * KEY_WIDTH, START_Y + 4 * KEY_HEIGHT);
        createFreeFret(instrument, Note.E, 3, START_X + 12 * KEY_WIDTH, START_Y + 5 * KEY_HEIGHT);

    }

    private void createFreeFret(Instrument instrument, Note n, int octave, double poxX, double posY) {

        InstrumentKey key = instrument.addInstrumentKey();

        key.getShape().setPosition(new Point(poxX, posY));
        key.getShape().setWidth(KEY_WIDTH);
        key.getShape().setHeight(KEY_HEIGHT);
        key.setBackgroundColor(FREE_FRET_COLOR);
        key.setSound(new GeneratedSound(n, octave - 1, TYPE));
        key.setBorder(new Border());
        key.getBorder().setWidth(0);
        key.setPersistance(1000);
    }

    private void drawFretSeparators(Instrument instrument) {
        InstrumentKey key;
        for (int i = 1; i <= 12; i++) {
            key = instrument.addInstrumentKey();
            key.getShape().setPosition(new Point(START_X - KEY_WIDTH / 2 + KEY_WIDTH * i - FRET_SEPARATOR_WIDTH / 2, 0.5));
            key.getShape().setHeight(0.5);
            key.getShape().setWidth(FRET_SEPARATOR_WIDTH);
            key.setBackgroundColor(FRET_SEPARATOR_COLOR);
            key.setPlayable(false);
        }
    }

    private void drawCircles(Instrument instrument) {
        int[] frets = new int[]{2, 4, 6, 8};

        for (int fret : frets) {
            drawCircle(instrument, START_X + fret * KEY_WIDTH, 0.5);
        }

        drawCircle(instrument, START_X + 11 * KEY_WIDTH, START_Y + KEY_HEIGHT + KEY_HEIGHT / 2);
        drawCircle(instrument, START_X + 11 * KEY_WIDTH, START_Y + KEY_HEIGHT * 3 + KEY_HEIGHT / 2);
    }

    private void drawCircle(Instrument instrument, double posx, double posy) {
        InstrumentKey key = instrument.addInstrumentKey();
        Shape s = new Ellipse(MARKER_DIAMETER, MARKER_DIAMETER, posx, posy);
        key.setShape(s);
        key.setPlayable(false);
        key.getBorder().setWidth(0);
        key.setBackgroundColor(MARKER_COLOR);
    }
}
