/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.template;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.Note;
import gaudrophone.instrument.apparence.Color;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.sound.Sound;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.sound.generated.GeneratedSoundInstrument;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sunny
 */
public class PianoTemplateStrategy implements InstrumentTemplateStrategy {

    private static final double PIANO_WIDTH = 1.0;
    private static final double WHITE_KEY_WIDTH = PIANO_WIDTH / 52;
    private static final double WHITE_KEY_HEIGHT = 0.35;
    private static final double TOP_BORDER_HEIGHT = 0.03;

    private static final double BLACK_KEY_WIDTH = WHITE_KEY_WIDTH / 2;
    private static final double BLACK_KEY_HEIGHT = WHITE_KEY_HEIGHT * 2 / 3;

    private static final Color WHITE_KEY_COLOR = new Color(0xFFFFFFF0);
    private static final Color BLACK_KEY_COLOR = new Color(0xFF100c08);

    private static final GeneratedSoundInstrument INSTRUMENT = GeneratedSoundInstrument.PIANO;

    @Override

    public Instrument generateInstrument() {
        Instrument instrument = new Instrument();
        instrument.setName(toString());

        generateWhiteKeys(instrument);
        generateBlackKeys(instrument);
        drawTopBorder(instrument);

        return instrument;
    }

    @Override
    public String toString() {
        return "Piano";
    }

    // http://www.harmoniumnet.nl/klavier-keyboard-ENG.html
    private void generateWhiteKeys(Instrument instrument) {

        double offsetX = WHITE_KEY_WIDTH / 2;

        createWhiteKey(instrument, Note.A, 0, offsetX);
        offsetX += WHITE_KEY_WIDTH;
        createWhiteKey(instrument, Note.B, 0, offsetX);
        offsetX += WHITE_KEY_WIDTH;

        List<Note> notes = getNotesWithoutSharps();

        for (int i = 1; i < 8; i++) {
            for (Note n : notes) {
                createWhiteKey(instrument, n, i, offsetX);
                offsetX += WHITE_KEY_WIDTH;
            }
        }
        createWhiteKey(instrument, Note.C, 8, offsetX);
    }

    private List<Note> getNotesWithoutSharps() {
        return Arrays.asList(Note.values()).stream().filter(m -> !m.toString().contains("#")).collect(Collectors.toList());
    }

    private List<Note> getSharpNotes() {
        return Arrays.asList(Note.values()).stream().filter(m -> m.toString().contains("#")).collect(Collectors.toList());
    }

    private void createWhiteKey(Instrument instrument, Note n, int octave, double posX) {

        InstrumentKey key = instrument.addInstrumentKey();

        Sound s = new GeneratedSound(n, octave, INSTRUMENT);
        key.setSound(s);

        key.setBackgroundColor(WHITE_KEY_COLOR);
        key.getShape().setHeight(WHITE_KEY_HEIGHT);
        key.getShape().setWidth(WHITE_KEY_WIDTH);
        key.getShape().setPosition(new Point(posX, 0.5));
        key.getSound().setPersistence(1000);
    }

    private void generateBlackKeys(Instrument instrument) {

        double offsetX = WHITE_KEY_WIDTH;

        createBlackKey(instrument, Note.A_SHARP, 0, offsetX);
        offsetX += 2 * WHITE_KEY_WIDTH;

        List<Note> notes = getSharpNotes();

        for (int i = 1; i < 8; i++) {
            for (int j = 0; j < 2; j++) {
                createBlackKey(instrument, notes.get(j), i, offsetX);
                offsetX += WHITE_KEY_WIDTH;
            }
            offsetX += WHITE_KEY_WIDTH;
            for (int j = 2; j < 5; j++) {
                createBlackKey(instrument, notes.get(j), i, offsetX);
                offsetX += WHITE_KEY_WIDTH;
            }
            offsetX += WHITE_KEY_WIDTH;
        }
    }

    private void createBlackKey(Instrument instrument, Note n, int octave, double posX) {

        double posY = 0.5 - BLACK_KEY_HEIGHT / 4;

        InstrumentKey key = instrument.addInstrumentKey();

        Sound s = new GeneratedSound(n, octave, INSTRUMENT);
        key.setSound(s);

        key.setBackgroundColor(BLACK_KEY_COLOR);
        key.getShape().setHeight(BLACK_KEY_HEIGHT);
        key.getShape().setWidth(BLACK_KEY_WIDTH);
        key.getShape().setPosition(new Point(posX, posY));
        key.getSound().setPersistence(1000);
    }

    private void drawTopBorder(Instrument instrument) {

        InstrumentKey key = instrument.addInstrumentKey();
        key.setPlayable(false);
        key.setBackgroundColor(BLACK_KEY_COLOR);
        key.getShape().setHeight(TOP_BORDER_HEIGHT);
        key.getShape().setWidth(PIANO_WIDTH);
        key.getShape().setPosition(new Point(PIANO_WIDTH / 2, 0.5 - (WHITE_KEY_HEIGHT / 2 + TOP_BORDER_HEIGHT / 2)));
    }
}
