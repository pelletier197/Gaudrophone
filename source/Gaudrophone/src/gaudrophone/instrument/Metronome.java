/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument;

import gaudrophone.instrument.sound.Sound;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.sound.generated.GeneratedSoundInstrument;
import gaudrophone.instrument.sound.generated.GeneratedSoundManager;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author sunny
 */
public class Metronome {

    public static final int MAX_BPM = 220;
    public static final int MIN_BMP = 40;

    public static final int DEFAULT_BPM = 80;

    private static final long MINUTE_IN_MS = 60_000;

    private Timer timer;
    private int bpm;
    private Sound sound;
    private boolean running;

    public Metronome() {
        timer = new Timer();
        this.bpm = DEFAULT_BPM;

        this.sound = new GeneratedSound(Note.C, 3, GeneratedSoundInstrument.DRUM);
    }

    public void open() {
        if (!running) {
            createTimerTask();
            running = true;
        }
    }

    public void close() {
        if (running) {
            cancelTimerTask();
            running = false;
        }
    }

    public void setBPM(int bpm) {
        
        if (bpm < MIN_BMP || bpm > MAX_BPM) {
            throw new IllegalArgumentException("BPM out of range");
        }
        
        this.bpm = bpm;

        if (running) {
            cancelTimerTask();
            createTimerTask();
        }
    }

    private void cancelTimerTask() {
        timer.cancel();
        timer = new Timer();
    }

    private void createTimerTask() {

        final long interval = MINUTE_IN_MS / bpm;
        timer.scheduleAtFixedRate(getTimerTask(), 200, interval);
    }

    private TimerTask getTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                sound.play();
                sound.stop();
            }
        };
    }
}
