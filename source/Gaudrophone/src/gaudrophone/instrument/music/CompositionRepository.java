/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Note;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

/**
 *
 * @author olivi
 */
public class CompositionRepository {

    private final double DEFAULT_DURATION = 1;

    //private CompositionBuilder compositionBuilder;
    
    public Composition parseFile(String filePath) throws FileNotFoundException {
        List<String> lines = readAllLines(filePath);
        String fileName = getCompositionName(filePath);
        Composition composition = parseStringList(lines, fileName);
        composition.setScore(createCompositionPartition(lines));
        
        return composition;
    }
    
    public Composition parseStringList(List<String> stringList, String compositionName) {
        CompositionBuilder compositionBuilder = new CompositionBuilder();
        boolean bpmLineRead = false;
        
        Paragraph paragraph = new Paragraph();
        for (Iterator<String> stringIt = stringList.iterator(); stringIt.hasNext();) {
            String rawLine = cleanRawLine(stringIt.next());
            rawLine = cleanRawLine(rawLine);
            // Ignore comment line
            if (rawLine.startsWith("//")) {
                continue;
            }
            
            // Set a bpm on the builder if there is one specified
            if (!bpmLineRead && !rawLine.isEmpty()) {
                int bpm = Integer.parseInt(rawLine);
                compositionBuilder.setRhythm(bpm);
                bpmLineRead = true;
                continue;
            }
            
            // add a line the the paragrah if it is in a paragraph
            if (!rawLine.isEmpty()) {
                paragraph.addLine(rawLine);
            }
            
            // parse the paragraph if a paragraph ends or at the end of the file
            if (rawLine.isEmpty() || !stringIt.hasNext()) {
                paragraph.writeToBuilder(compositionBuilder);
                paragraph.clear();
            }
        }

        return compositionBuilder.build(compositionName);
    }
    
    public String cleanRawLine(String input) {
        input = input.replaceAll("\\|", "");
        input = input.trim();
        return input;
    }

    private String getCompositionName(String path) {
        String name = new File(path).getName();
        name = name.substring(0, name.lastIndexOf("."));
        return name;
    }

    private SpecificNote getNoteFromString(String noteString) {
        // TODO support bemol
        if (noteString.equals("X") || noteString.equals("x")) {
            return null;
        }

        SpecificNote specificNote = new SpecificNote();
        specificNote.note = Note.getNoteFromEnglishName(getNonNumericalCharacters(noteString));

        String octaveString = getNumericalCharacters(noteString);
        int octave;

        if (octaveString.length() > 0) {
            octave = Integer.parseInt(octaveString);
        } else {
            octave = 4;
        }

        specificNote.octave = octave;
        return specificNote;
    }

    private String getNonNumericalCharacters(String str) {
        return str.replaceAll("[0-9]*", "");
    }

    private String getNumericalCharacters(String str) {
        return str.replaceAll("[^0-9]*", "");
    }

    private double getDurationFromString(String durationString) {
        
        double scale = durationString.length() == 2 ? 1.5 : 1;

        String durationSubString = durationString.substring(0, 1);

        switch (durationSubString) {
            case ":":
                return 0.0625 * scale;
            case ";":
                return 0.125 * scale;
            case ".":
                return 0.25 * scale;
            case ",":
                return 0.5 * scale;
            case "_":
                return 1 * scale;
            default:
                return Double.parseDouble(durationString) * scale;
        }
    }

    private boolean isTimeLine(String line) {
        return line.matches("(((\\d+.?\\d*)|([_,\\.:;]))(\\.)?((\\s+|$)))*");
    }

    private boolean isMusicLine(String line) {
        // TODO support bemol
        return line.matches("(((?i)[CDEFGABX])([\\d]{0,1})(#{0,1})(\\s+|$))*");
    }

    private List<String> readAllLines(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        BufferedReader reader = new BufferedReader(new FileReader(file));

        return reader.lines().collect(Collectors.toList());
    }

    private String createCompositionPartition(List<String> fileLines) {

        List<List<String>> paragraphs = new ArrayList<>();
        List<String> currentParagraph = new ArrayList<>();

        for (String s : fileLines) {
            s = s.trim();
            if (s.isEmpty()) {

                if (!currentParagraph.isEmpty()) {
                    paragraphs.add(currentParagraph);
                    currentParagraph = new ArrayList<>();
                }
            } else {
                currentParagraph.add(s);
            }
        }

        // Adds the last data
        if (!currentParagraph.isEmpty()) {
            paragraphs.add(currentParagraph);
            currentParagraph = new ArrayList<>();
        }

        StringBuilder builder = new StringBuilder();

        // Spaces all paragraphs equal
        paragraphs = paragraphs.stream().map(m -> m.stream().map(k -> spaceEqual(k)).collect(Collectors.toList())).collect(Collectors.toList());
        final int maxNumberOfLinesPerParagraph = paragraphs.stream().map(m -> m.size()).max(Comparator.naturalOrder()).get();
        final List<Integer> widthsPerParagraphs = paragraphs.stream().map(m -> m.stream().map(k -> k.length()).max(Comparator.naturalOrder()).get()).collect(Collectors.toList());

        for (int i = 0; i < maxNumberOfLinesPerParagraph; i++) {

            builder.append("\n");

            for (int j = 0; j < paragraphs.size(); j++) {

                List<String> pLines = paragraphs.get(j);

                if (i < pLines.size() - 1) {
                    builder.append(completeMissingChar(pLines.get(i), widthsPerParagraphs.get(j)));
                } else {
                    if (i == maxNumberOfLinesPerParagraph - 1) {
                        builder.append(completeMissingChar(pLines.get(pLines.size() - 1), widthsPerParagraphs.get(j)));
                    } else {
                        builder.append(String.join("", Collections.nCopies(widthsPerParagraphs.get(j), " ")));
                    }
                }
            }
        }

        return builder.toString();
    }

    private String completeMissingChar(String s, int requiredLength) {

        int missingChar = requiredLength - s.length();
        if (missingChar > 0) {
            return s + String.join("", Collections.nCopies(missingChar, " "));
        }
        return s;
    }

    private String spaceEqual(String s) {

        if (s.trim().isEmpty()) {
            return "";
        }

        if (s.trim().startsWith("//")) {
            return "  " + s;
        }

        String[] parts = s.split(" ", -2);

        final char space = ' ';
        StringBuilder builder = new StringBuilder();

        for (String part : parts) {
            switch (part.length()) {
                case 3:
                    builder.append(space).append(part);
                    break;
                case 2:
                    builder.append(space).append(space).append(part);
                    break;
                case 1:
                    builder.append(space).append(space).append(part).append(space);
                    break;
            }
        }

        return builder.toString();
    }
    
    private class Paragraph {
        
        List<SubParagraph> subParagraphs = new ArrayList<>();
        int writingIndex;
        
        public Paragraph() {
            writingIndex = 0;
            subParagraphs.add(new SubParagraph());
        }
        
        public void addLine(String line) {
            boolean validLine = false;
            
            String[] tempLineElements = line.split(" ");
            ArrayList<String> lineElements = new ArrayList<>();
            
            for (String element : tempLineElements) {
                if (!element.isEmpty()) 
                    lineElements.add(element);
            }
            
            SubParagraph activeSubParagraph = subParagraphs.get(subParagraphs.size() - 1);
            if (isMusicLine(line)) {
                ArrayList<SpecificNote> noteLine = new ArrayList<>();
                for (String noteString : lineElements) {
                    SpecificNote specificNote = getNoteFromString(noteString.toUpperCase());
                    noteLine.add(specificNote);
                }
                activeSubParagraph.musicLines.add(noteLine);
                validLine = true;
            } else if (isTimeLine(line)) {
                ArrayList<Double> durations = new ArrayList<>();
                for (String durationString : lineElements) {
                    Double duration = getDurationFromString(durationString);
                    durations.add(duration);
                }
                activeSubParagraph.timeLine = durations;
                subParagraphs.add(new SubParagraph());
                validLine = true;
            }
            
            if (!validLine) {
                throw new IllegalArgumentException("The format is invalid");
            }
        }
        
        public void writeToBuilder(CompositionBuilder compositionBuilder) {
            if (subParagraphs.isEmpty())
                return;
            
            Comparator<NoteEvent> comparator = new NoteEventTimeComparator();
            PriorityQueue<NoteEvent> noteEvents = new PriorityQueue(11, comparator);
            for (SubParagraph subParagraph : subParagraphs) {
                for (ArrayList<SpecificNote> musicLine : subParagraph.musicLines) {
                    Double timeFromLineStart = 0.0;
                    for (int specificNoteIndex = 0; specificNoteIndex < musicLine.size(); specificNoteIndex++) {
                        Double duration = DEFAULT_DURATION;
                        if (subParagraph.timeLine != null) {
                            duration = subParagraph.timeLine.get(specificNoteIndex);
                        }
                        
                        if (musicLine.get(specificNoteIndex) == null) {
                            NoteEvent noteEventPauseStart = new NoteEvent();
                            noteEventPauseStart.eventTime = timeFromLineStart;
                            noteEventPauseStart.type = MusicalEventType.PAUSE;
                            //noteEventPause.note = note;
                            //noteEventPause.octave = octave;
                            
                            timeFromLineStart += duration;
                            NoteEvent noteEventPauseStop = new NoteEvent();
                            noteEventPauseStop.eventTime = timeFromLineStart;
                            noteEventPauseStop.type = MusicalEventType.PAUSE;

                            noteEvents.add(noteEventPauseStart);
                            noteEvents.add(noteEventPauseStop);
                        } else {
                            Note note = musicLine.get(specificNoteIndex).note;
                            int octave = musicLine.get(specificNoteIndex).octave;

                            NoteEvent noteEventStart = new NoteEvent();
                            noteEventStart.eventTime = timeFromLineStart;
                            noteEventStart.type = MusicalEventType.KEY_PRESS;
                            noteEventStart.note = note;
                            noteEventStart.octave = octave;

                            timeFromLineStart += duration;
                            NoteEvent noteEventStop = new NoteEvent();
                            noteEventStop.eventTime = timeFromLineStart;
                            noteEventStop.type = MusicalEventType.KEY_RELEASE;
                            noteEventStop.note = note;
                            noteEventStop.octave = octave;

                            noteEvents.add(noteEventStart);
                            noteEvents.add(noteEventStop);
                        }
                    }
                }
            }
            
            Double lastEventTime = 0.0;
            while (!noteEvents.isEmpty()) {
                NoteEvent noteEvent = noteEvents.remove();
                // Close the current frame before writing in a new one
                if (noteEvent.eventTime > lastEventTime) {
                    Double duration = noteEvent.eventTime - lastEventTime;
                    writingIndex++;
                    compositionBuilder.makePause(writingIndex);
                    compositionBuilder.setFrameDuration(writingIndex, duration);
                    lastEventTime = noteEvent.eventTime;
                    writingIndex++;
                }
                if (noteEvent.type == MusicalEventType.KEY_PRESS || noteEvent.type == MusicalEventType.KEY_RELEASE) {
                        compositionBuilder.createNote(writingIndex, noteEvent.type, noteEvent.note, noteEvent.octave);
                }
            }
        }
        
        public void clear() {
            subParagraphs.clear();
            subParagraphs.add(new SubParagraph());
        }
        
        private class NoteEvent {
            Double eventTime;
            MusicalEventType type;
            Note note;
            int octave;
        }
        
        private class NoteEventTimeComparator implements Comparator<NoteEvent> {
            @Override
            public int compare(NoteEvent a, NoteEvent b) {
                if (a.eventTime < b.eventTime) {
                    return -1;
                }
                if (a.eventTime > b.eventTime) {
                    return 1;
                }
                if (a.type != b.type) {
                    if (a.type == MusicalEventType.KEY_RELEASE) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
                return 0;
            }
        }
        
        private class SubParagraph {
            
            public ArrayList<ArrayList<SpecificNote>> musicLines = new ArrayList<>();
            public ArrayList<Double> timeLine;
        }
    }
    
    private class SpecificNote {
        
        public Note note;
        public int octave;
    }
}
