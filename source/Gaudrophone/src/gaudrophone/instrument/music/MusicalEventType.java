/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

/**
 *
 * @author sunny
 */
public enum MusicalEventType {
    KEY_PRESS, KEY_RELEASE, PAUSE;
}
