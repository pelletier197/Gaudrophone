/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import java.io.Serializable;

/**
 *
 * @author sunny
 */
public class MusicalEvent implements Serializable {

    public final MusicalEventType type;
    public final Object data;

    public MusicalEvent(MusicalEventType type, Object data) {
        this.type = type;
        this.data = data;
    }
    
    
}
