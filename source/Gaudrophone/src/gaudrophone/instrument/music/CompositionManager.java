/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.IO.GaudrophoneIOManager;
import gaudrophone.instrument.template.GuitarTemplateStrategy;
import gaudrophone.instrument.template.PianoTemplateStrategy;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Stéphanie Mercier
 */
public class CompositionManager {
    private static CompositionManager instance;

    public static CompositionManager getInstance() {
        if (instance == null) {
            instance = new CompositionManager();
        }

        return instance;
    }

    private final GaudrophoneIOManager ioManager;
    
    private CompositionManager() {
        this.ioManager = new GaudrophoneIOManager();
    }
    
    public Set<String> getCompositionNames() {
        return ioManager.getCompositionNames();
    }
    
    public boolean deleteCompositionFromName(String name){
        return ioManager.deleteCompositionFromName(name);
    }
    
    public void saveComposition(Composition instrument) {
        ioManager.saveComposition(instrument);
    }
    
    public Composition getCompositionFromName(String name){
        return ioManager.loadCompositionFromName(name);
    }
}
