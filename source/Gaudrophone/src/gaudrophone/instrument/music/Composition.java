/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Loup
 */
public class Composition implements Serializable {

    public static long MAX_PAUSE_TIME = 250;

    private List<MusicalEvent> events;

    public long duration;
    private final String name;
    private final UUID uuid;

    private String score;

    public Composition(List<MusicalEvent> events, String name) {
        if (events == null) {
            throw new NullPointerException("Invalid events");
        }
        events = computeEvents(events);
        this.events = new ArrayList<>(events);
        this.duration = computeDuration(events);
        this.uuid = UUID.randomUUID();
        this.name = name;
        this.score = "";
    }

    public MusicalEvent eventAtFrame(int frame) {
        return events.get(frame);
    }

    public int numberOfFrames() {
        return events.size();
    }

    public long getDuration() {
        return duration;
    }

    private List<MusicalEvent> computeEvents(List<MusicalEvent> events) {
        List<MusicalEvent> newEventList = new ArrayList<>();
        events.forEach((e) -> {
            switch (e.type) {
                case PAUSE:
                    long pauseTime = (long) e.data;
                    while (pauseTime > MAX_PAUSE_TIME) {
                        newEventList.add(new MusicalEvent(MusicalEventType.PAUSE, MAX_PAUSE_TIME));
                        pauseTime -= MAX_PAUSE_TIME;
                    }
                    newEventList.add(new MusicalEvent(MusicalEventType.PAUSE, pauseTime));
                    break;
                default:
                    newEventList.add(e);
                    break;
            }
        });
        return newEventList;
    }

    private long computeDuration(List<MusicalEvent> events) {
        long duration = 0;

        // Duration is the total of pauses
        for (MusicalEvent m : events) {
            if (m.type == MusicalEventType.PAUSE) {
                duration += (long) m.data;
            }
        }

        return duration;
    }

    int seekFrame(long playedTime) {
        if (playedTime > duration) {
            throw new IllegalArgumentException("The seeked frame is not in the composition.");
        }

        if (playedTime == 0) {
            return 0;
        }
        
        long timeTravelled = 0;
        int frame = 0;
        MusicalEvent event;
        while (true) {
            event = events.get(frame);
            if (event.type != MusicalEventType.PAUSE) {
                frame++;
                continue;
            }

            timeTravelled += (long) event.data;

            if (playedTime < timeTravelled) {
                return frame;
            }
            frame++;
        }
    }

    public String getName() {
        return this.name;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getScore() {
        return score;
    }
}
