/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.IO;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.music.Composition;
import gaudrophone.instrument.utils.UrlDecoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author sunny
 */
public class GaudrophoneIOManager {

    private static final String NO_NAME = "Sans nom";

    private static final String INSTRUMENT_PATH = Paths.get(System.getenv("APPDATA"), "Gaudrophone", "instruments").toString();
    private static final String COMPOSITION_PATH = Paths.get(System.getenv("APPDATA"), "Gaudrophone", "compositions").toString();
    private static final String INSTRUMENT_FILE_EXTENSION = ".instr";
    private static final String COMPOSITION_FILE_EXTENSION = ".compo";

    public GaudrophoneIOManager() {
        // Creates the app data file to save the instruments
        File base = new File(INSTRUMENT_PATH);
        File composition = new File(COMPOSITION_PATH);

        if (!base.exists()) {
            base.mkdirs();
        }

        if (!composition.exists()) {
            composition.mkdirs();
        }
    }

    public Set<String> getCustomInstrumentNames() {
        return getFileNames(INSTRUMENT_PATH, INSTRUMENT_FILE_EXTENSION);
    }

    private void insertWithUniqueName(Set<String> names, String name) {
        int index = 1;
        String test = name;

        while (names.contains(test)) {
            test = String.format("%s (%d)", name, index);
            index++;
        }

        names.add(test);
    }

    public boolean deleteInstrumentFromName(String name) {
        return deleteFileFromName(name, INSTRUMENT_PATH, INSTRUMENT_FILE_EXTENSION);
    }

    public boolean deleteCompositionFromName(String name) {
        return deleteFileFromName(name, COMPOSITION_PATH, COMPOSITION_FILE_EXTENSION);
    }
    
    private boolean deleteFileFromName(String name, String basePath, String extension) {
        File file = findFileFromName(name, basePath, extension);
        try {
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Instrument loadInstrumentFromName(String name) {
        return (Instrument) loadObjectFromName(name, INSTRUMENT_PATH, INSTRUMENT_FILE_EXTENSION);
    }

    public Composition loadCompositionFromName(String name) {
        return (Composition) loadObjectFromName(name, COMPOSITION_PATH, COMPOSITION_FILE_EXTENSION);
    }

    private Object loadObjectFromName(String name, String basePath, String extension) {
        File file = findFileFromName(name, basePath, extension);

        try {
            ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));
            Object result = stream.readObject();
            stream.close();

            return result;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private File findFileFromName(String name, String basePath, String extension) {
        Pattern p = Pattern.compile("^[0-9a-zA-Z ]* \\(([0-9]+)\\)$");
        Matcher m = p.matcher(name);

        // The name refers to the first file
        if (!m.find()) {
            return findFileNumberXThatMatches(0, name, basePath, extension);
        } else {
            int number = Integer.parseInt(m.group(1));
            return findFileNumberXThatMatches(number, name.substring(0, name.lastIndexOf(' ')), basePath, extension);
        }
    }

    private File findFileNumberXThatMatches(int fileNumber, String name, String basePath, String extension) {
        Pattern p = Pattern.compile("^" + Pattern.quote(name) + " [\\d\\w\\-]{36}" + Pattern.quote(extension) + "$");

        int count = 0;

        File base = new File(basePath);

        File[] objectFiles = base.listFiles();

        for (File f : objectFiles) {
            if (p.matcher(f.getName()).matches()) {
                if (count == fileNumber) {
                    return f;
                }
                count++;
            }
        }

        return null;
    }

    public void saveInstrument(Instrument instr) {
        saveObject(instr, instr.getUuid(), instr.getName(), INSTRUMENT_PATH, INSTRUMENT_FILE_EXTENSION);
    }

    public void saveComposition(Composition composition) {
        saveObject(composition, composition.getUuid(), composition.getName(), COMPOSITION_PATH, COMPOSITION_FILE_EXTENSION);
    }
    
    private void saveObject(Object object, UUID uuid, String name, String basePath, String extension) {
        
        if (name == null || name.trim().isEmpty()) {
            name = NO_NAME;
        }

        File f = findFile(uuid, basePath, extension);
        if (f != null) {
            f.delete();
        }

        f = fileForObject(name, uuid, basePath, extension);

        ObjectOutputStream stream;
        try {
            stream = new ObjectOutputStream(new FileOutputStream(f));
            stream.writeObject(object);

            stream.flush();
            stream.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private File findFile(UUID uuid, String basePath, String extension) {
        File base = new File(basePath);
        File[] objectFiles = base.listFiles();

        final String end = uuid.toString() + extension;

        for (File f : objectFiles) {
            if (f.getName().endsWith(end)) {

                return f;
            }
        }
        return null;
    }

    public Set<String> getCompositionNames() {
        return getFileNames(COMPOSITION_PATH, COMPOSITION_FILE_EXTENSION);
    }

    private Set<String> getFileNames(String path, String extension) {
        File base = new File(path);
        Set<String> names = new TreeSet<>();

        File[] compositionsFiles = base.listFiles();

        for (File f : compositionsFiles) {
            if (f.getName().endsWith(extension)) {
                insertWithUniqueName(names, adjustObjectName(f.getName(), extension));
            }
        }

        return names;
    }

    private File formatToObjectFile(String name, String basePath, String extension) {
        return new File(basePath + "/" + name + extension);
    }

    private String adjustObjectName(String fileName, String extension) {
        return fileName.replace(extension, "").substring(0, fileName.lastIndexOf(' '));
    }

    private File fileForObject(String name, UUID uuid, String basePath, String extension) {
        return formatToObjectFile(name + " " + uuid.toString(), basePath, extension);
    }

}
