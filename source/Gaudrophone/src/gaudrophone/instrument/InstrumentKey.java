/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument;

import gaudrophone.instrument.apparence.Border;
import gaudrophone.instrument.apparence.Color;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;
import gaudrophone.instrument.geometry.Shape;
import gaudrophone.instrument.geometry.shapes.Rectangle;
import gaudrophone.instrument.sound.Sound;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Playable Key of an instrument.
 *
 * @author sunny
 */
public final class InstrumentKey implements Serializable {

    private static final Color DEFAULT_COLOR = new Color(0xfff2f2f2); // Dark white
    private static final String DEFAULT_NAME = "Sans nom";

    //-note : Note
    private Sound sound;
    private Border border;
    //-couleurFond : Couleur
    private Color backgroundColor;
    //-nom : string
    private String name;
    //-forme : Forme
    private Shape shape;

    private UUID uuid;

    private boolean pressed;
    private boolean demoPressed;
    private boolean playable;

    private String imageFile;
    private transient BufferedImage image;

    private boolean volumeDisplayed;
    private boolean nameDisplayed;
    private boolean noteAndOctaveDisplayed;
    private boolean persistanceDisplayed;
    private boolean frequencyDisplayed;

    /**
     * Constructor of a default InstrumentKey.
     */
    public InstrumentKey() {
        this.uuid = UUID.randomUUID();

        setSound(new GeneratedSound());
        setBorder(new Border());
        setBackgroundColor(DEFAULT_COLOR);
        setName(DEFAULT_NAME);
        setShape(new Rectangle());
        playable = true;
    }

    /**
     * The shape of the InstrumentKey is returned.
     *
     * @return The shape of the InstrumentKey
     */
    public Shape getShape() {
        return shape;
    }

    /**
     *
     * @param shape Shape to set.
     */
    public void setShape(Shape shape) {
        this.shape = shape;
    }

    /**
     * The name of the InstrumentKey is returned.
     *
     * @return The name of the InstrumentKey
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The sound of the InstrumentKey is returned.
     *
     * @return The sound of the InstrumentKey
     */
    public Sound getSound() {
        return sound;
    }

    /**
     *
     * @param sound sound to set.
     */
    public void setSound(Sound sound) {
        this.sound = sound;
    }

    /**
     * The border of the InstrumentKey is returned.
     *
     * @return The border of the InstrumentKey
     */
    public Border getBorder() {
        return border;
    }

    /**
     *
     * @param border border to set.
     */
    public void setBorder(Border border) {
        this.border = border;
    }

    /**
     * The shape of the InstrumentKey is returned.
     *
     * @return The shape of the InstrumentKey
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    /**
     *
     * @param backgroundColor color to set.
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * Returns true if a position is inside the shape of the InstrumentKey.
     *
     * @param pPosition position to test against.
     * @return true if position is inside the shape of the InstrumentKey,
     * otherwise false
     */
    public boolean isInside(Point pPosition) {
        return this.shape.isInside(pPosition);
    }

    /**
     * Plays the sound of the InstrumentKey.
     */
    public void press() {
        if (isPlayable()) {
            this.sound.play();
            this.pressed = true;
        }
    }

    /**
     * Stops the sound of the InstrumentKey.
     */
    public void release() {
        this.sound.stop();
        this.pressed = false;
    }

    /**
     * Doesn't Plays the sound of the InstrumentKey.
     */
    public void pressDemo() {
        this.demoPressed = true;
    }

    /**
     * Doesn't Stops the sound of the InstrumentKey.
     */
    public void releaseDemo() {
        this.demoPressed = false;
    }

    /**
     * Move the shape of the IntrumentKey by the amount given in the Point
     * position.
     *
     * @param pDeltaPosition amount of translation in the plane
     */
    public void move(Point pDeltaPosition) {
        this.shape.translate(pDeltaPosition);
    }

    //+redimensionner(facteur : double) : void
    /**
     * Resizes the InstrumentKey shape by the factor given in parameter.
     *
     * @param pSizeFactor factor to resize by
     */
    public void resize(double pSizeFactor) {
        this.shape.scale(pSizeFactor);
    }

    /**
     *
     * @return True if the key is pressed, false otherwise
     */
    public boolean isPressed() {
        return pressed;
    }

    public boolean isPlayable() {
        return playable;
    }

    public void setPlayable(boolean clickable) {
        this.playable = clickable;
    }

    public boolean isVolumeDisplayed() {
        return volumeDisplayed && playable;
    }

    public void setVolumeDisplayed(boolean volumeDisplayed) {
        this.volumeDisplayed = volumeDisplayed;
    }

    public boolean isNameDisplayed() {
        return nameDisplayed && playable;
    }

    public void setNameDisplayed(boolean nameDisplayed) {
        this.nameDisplayed = nameDisplayed;
    }

    public boolean isNoteAndOctaveDisplayed() {
        return noteAndOctaveDisplayed && playable;
    }

    public void setNoteAndOctaveDisplayed(boolean noteAndOctaveDisplayed) {
        this.noteAndOctaveDisplayed = noteAndOctaveDisplayed;
    }

    public boolean isPersistanceDisplayed() {
        return persistanceDisplayed && playable;
    }

    public void setPersistanceDisplayed(boolean persistanceDisplayed) {
        this.persistanceDisplayed = persistanceDisplayed;
    }

    public void setImage(File file) throws IOException {
        if (file != null) {
            this.image = ImageIO.read(file);
            this.imageFile = file.getPath();
        } else {
            this.image = null;
            this.imageFile = null;
        }
    }

    public void setImage(String imageFile) throws IOException {
        if (!imageFile.startsWith("resource:")) {
            setImage(new File(imageFile));
        } else {
            this.image = ImageIO.read(getClass().getResourceAsStream(imageFile.replace("resource:", "")));
            this.imageFile = imageFile;
        }
    }

    public String getImageFile() {
        return imageFile;
    }

    public BufferedImage getImage() {
        if (image == null && imageFile != null) {
            try {
                setImage(imageFile);
            } catch (IOException ex) {
            }
        }
        return image;
    }

    public void setPersistance(long persistance) {
        this.sound.setPersistence(persistance);
    }

    public void setImage(BufferedImage image, String imageFile) {
        this.image = image;
        this.imageFile = imageFile;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %de octave", getName(), getSound().getNote(), getSound().getOctave());
    }

    public boolean isFrequencyDisplayed() {
        return frequencyDisplayed && playable;
    }

    public void setFrequencyDisplayed(boolean val) {
        this.frequencyDisplayed = val;
    }

    public boolean isDemoPressed() {
        return demoPressed;
    }

}
