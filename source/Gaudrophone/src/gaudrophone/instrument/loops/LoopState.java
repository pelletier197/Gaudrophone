/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.loops;

/**
 *
 * @author olivi
 */
public enum LoopState {
    RECORDING("Enregistrement"),
    PLAYING("Active"), 
    STOPPED("Vide");

    private String name;
    
    LoopState(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
}
