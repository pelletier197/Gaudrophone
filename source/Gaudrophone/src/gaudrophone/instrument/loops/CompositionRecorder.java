/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.loops;

import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.music.Composition;
import gaudrophone.instrument.music.FrameInfo;
import gaudrophone.instrument.music.MusicalEvent;
import gaudrophone.instrument.music.MusicalEventType;
import gaudrophone.instrument.utils.StringUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sunny
 */
public class CompositionRecorder {
    private static final int NAME_LENGTH = 10;

    private final List<MusicalEvent> events;

    private boolean recording;
    private boolean paused;

    private long lastEvent;
    private long pauseBeginTime;

    public CompositionRecorder() {
        this.events = new ArrayList<>();
    }

    public void startRecording() {
        if (recording) {
            return;
        }
        recording = true;
        paused = false;

        if (lastEvent == 0) {
            lastEvent = System.currentTimeMillis();
        }

        // Adjust the last event time after a pause
        if (pauseBeginTime != 0) {
            lastEvent += System.currentTimeMillis() - pauseBeginTime;
        }
    }

    public void pauseRecording() {
        if (paused) {
            return;
        }
        paused = true;
        pauseBeginTime = System.currentTimeMillis();
    }

    public void pressKey(InstrumentKey key) {
        checkStateForInput();
        long currentTime = System.currentTimeMillis();

        events.add(new MusicalEvent(MusicalEventType.PAUSE, currentTime - lastEvent));
        events.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(key.getSound().getNote(), key.getSound().getOctave())));

        lastEvent = currentTime;
    }

    public void releaseKey(InstrumentKey key) {
        checkStateForInput();
        long currentTime = System.currentTimeMillis();

        events.add(new MusicalEvent(MusicalEventType.PAUSE, currentTime - lastEvent));
        events.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(key.getSound().getNote(), key.getSound().getOctave())));

        lastEvent = currentTime;
    }

    private void checkStateForInput() {
        if (paused || !recording) {
            throw new IllegalStateException("Either on pause or not recording.");
        }
    }

    /**
     * Stops the recording and returns the composition created. The recorder is
     * then cleaned and can be used again.
     *
     * @return
     */
    public Composition stopRecording() {

        if (!recording) {
            throw new IllegalStateException("Not in the recording state");
        }

        if (paused) {
            events.add(new MusicalEvent(MusicalEventType.PAUSE, (pauseBeginTime - lastEvent)));
        } else {
            events.add(new MusicalEvent(MusicalEventType.PAUSE, System.currentTimeMillis() - lastEvent));
        }

        List<MusicalEvent> result = new ArrayList<>(events);

        events.clear();
        recording = false;
        paused = false;
        lastEvent = 0;
        pauseBeginTime = 0;

        return new Composition(result, StringUtils.generateRandomString(NAME_LENGTH));
    }

    public boolean isRecording() {
        return recording;
    }

    public boolean isPaused() {
        return paused;
    }

}
