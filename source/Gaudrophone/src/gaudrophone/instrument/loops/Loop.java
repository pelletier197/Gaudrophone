/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.loops;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.music.Composition;
import gaudrophone.instrument.music.MusicBox;
import gaudrophone.instrument.music.MusicalEventListener;

/**
 *
 * @author olivi
 */
public class Loop {

    private LoopState state;
    private MusicBox box;
    private CompositionRecorder recorder;

    public Loop(Instrument instr, MusicalEventListener listener) {
        this.state = LoopState.STOPPED;
        this.box = new MusicBox(instr);
        this.recorder = new CompositionRecorder();

        box.setMusicEventListener(listener);
    }

    public LoopState getState() {
        return state;
    }

    public void startPlaying() {
        if (recorder.isRecording()) {
            throw new IllegalStateException("Can't play while recording");
        }
        box.startLoop();
        this.state = LoopState.PLAYING;
    }

    public void stopPlaying() {
        if (state == LoopState.PLAYING) {
            box.stop();
            this.state = LoopState.STOPPED;
        }
    }

    public void startRecording() {
        if (box.isPlaying()) {
            throw new IllegalStateException("Can't record while playing");
        }
        this.recorder.startRecording();
        this.state = LoopState.RECORDING;
    }

    public void recordKeyPress(InstrumentKey key) {
        recorder.pressKey(key);
    }

    public void recordKeyRelease(InstrumentKey key) {
        recorder.releaseKey(key);
    }

    public void stopRecording() {
        this.box.setComposition(this.recorder.stopRecording());
        this.state = LoopState.STOPPED;
    }

    public void clear() {
        stopPlaying();
        recorder.startRecording();
        this.box.setComposition(null);
    }

}
