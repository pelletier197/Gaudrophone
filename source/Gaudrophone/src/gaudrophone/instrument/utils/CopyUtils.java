/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.utils;

import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.Note;
import gaudrophone.instrument.apparence.Border;
import gaudrophone.instrument.apparence.Color;
import gaudrophone.instrument.geometry.Ellipse;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;
import gaudrophone.instrument.geometry.Shape;
import gaudrophone.instrument.geometry.shapes.Diamond;
import gaudrophone.instrument.geometry.shapes.Hexagon;
import gaudrophone.instrument.geometry.shapes.Octogon;
import gaudrophone.instrument.geometry.shapes.Pentagon;
import gaudrophone.instrument.geometry.shapes.Rectangle;
import gaudrophone.instrument.geometry.shapes.Trapezoid;
import gaudrophone.instrument.geometry.shapes.Triangle;
import gaudrophone.instrument.sound.Sound;
import gaudrophone.instrument.sound.file.FileSound;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.sound.generated.GeneratedSoundInstrument;
import java.io.File;

/**
 *
 * @author sunny
 */
public class CopyUtils {
    
    public static InstrumentKey copy(InstrumentKey key) {
        
        InstrumentKey copy = new InstrumentKey();
        copy.setSound(copy(key.getSound()));
        copy.setShape(copy(key.getShape()));
        
        copy.setBackgroundColor(copy(key.getBackgroundColor()));
        copy.setImage(key.getImage(), key.getImageFile());
        copy.setBorder(copy(key.getBorder()));
        copy.setName(key.getName());
        
        copy.setNameDisplayed(key.isNameDisplayed());
        copy.setNoteAndOctaveDisplayed(key.isNoteAndOctaveDisplayed());
        copy.setPersistanceDisplayed(key.isPersistanceDisplayed());
        copy.setVolumeDisplayed(key.isVolumeDisplayed());
        copy.setFrequencyDisplayed(key.isFrequencyDisplayed());
        
        copy.setPlayable(key.isPlayable());
        
        return copy;
    }
    
    public static Border copy(Border border) {
        
        Border copy = new Border();
        copy.setColor(copy(border.getColor()));
        copy.setType(border.getType());
        copy.setWidth(border.getWidth());
        
        return copy;
    }
    
    public static Color copy(Color color) {
        return new Color(color.getAlpha(), color.getRed(), color.getGreen(), color.getBlue());
    }
    
    public static Sound copy(Sound s) {
        Sound newSound = null;
        
        if (s instanceof GeneratedSound) {
            GeneratedSound source = (GeneratedSound) s;
            newSound = new GeneratedSound(source.getNote(), source.getOctave(), source.getInstrument());
            
        } else {
            FileSound source = (FileSound) s;
            newSound = new FileSound();
            ((FileSound) newSound).setFilePath(source.getFilePath());
        }
        
        copyProperties(s, newSound);
        return newSound;
    }
    
    public static void copyProperties(Sound from, Sound to) {
        to.setPersistence(from.getPersistence());
        to.setNote(from.getNote());
        to.setOctave(from.getOctave());
        to.setVolume(from.getVolume());
    }
    
    private static Shape copy(Shape shape) {
        
        Shape copy = null;
        
        if (shape instanceof Ellipse) {
            copy = new Ellipse(shape.getWidth(), shape.getHeight(), shape.getCenter());
        } else {
            copy = copyPolygon(shape);
        }
        
        copy.setHeight(shape.getHeight());
        copy.setWidth(shape.getWidth());
        copy.setPosition(shape.getCenter());
        
        return copy;
    }
    
    private static Shape copyPolygon(Shape shape) {
        if (shape instanceof Diamond) {
            return new Diamond();
        } else if (shape instanceof Hexagon) {
            return new Hexagon();
        } else if (shape instanceof Octogon) {
            return new Octogon();
        } else if (shape instanceof Pentagon) {
            return new Pentagon();
        } else if (shape instanceof Rectangle) {
            return new Rectangle();
        } else if (shape instanceof Trapezoid) {
            return new Trapezoid();
        } else if (shape instanceof Triangle) {
            return new Triangle();
        } else {
            Point[] points = shape.getPoints();
            Point[] copy = new Point[points.length];
            
            for (int i = 0; i < points.length; i++) {
                copy[i] = new Point(points[i]);
            }
            
            return new Polygon(copy);
        }
    }
}
