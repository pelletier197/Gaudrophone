/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument;

import java.util.ArrayList;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Shape;
import gaudrophone.instrument.geometry.shapes.Rectangle;
import gaudrophone.instrument.search.InstrumentKeyNameSearchStrategy;
import gaudrophone.instrument.search.InstrumentKeySearchStrategy;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.utils.CopyUtils;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Musical instrument in the Gaudrophone application.
 *
 * @author sunny
 */
public class Instrument implements Serializable {

    private static final double DEFAULT_KEY_HEIGHT = 0.1;
    private static final double DEFAULT_KEY_WIDTH = 0.1;
    private InstrumentState state;
    private List<InstrumentKey> selectedKeys;
    private String name;
    private final LinkedList<InstrumentKey> instrumentKeys;
    private transient InstrumentKeySearchStrategy searchStrategy;

    private UUID uuid;

    public InstrumentState getState() {
        return state;
    }

    public void setState(InstrumentState state) {
        this.state = state;

        if (!selectedKeys.isEmpty()) {
            this.selectedKeys.forEach(m -> m.release());
            this.selectedKeys.clear();
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<InstrumentKey> getSelectedKeysz() {
        return selectedKeys;
    }

    public InstrumentKey getSelectedKey() {
        return selectedKeys.isEmpty() ? null : selectedKeys.get(0);
    }

    public List<InstrumentKey> getInstrumentKeys() {
        return this.instrumentKeys;
    }

    /**
     * Creates an empty Instrument.
     */
    public Instrument() {
        instrumentKeys = new LinkedList<>();
        this.searchStrategy = new InstrumentKeyNameSearchStrategy();
        this.uuid = UUID.randomUUID();
        this.selectedKeys = new ArrayList<>();
    }

    public UUID getUuid() {
        return uuid;
    }

    /**
     * Adds a default InstrumentKey to the Instrument.
     *
     * @return The new InstrumentKey
     */
    public InstrumentKey addInstrumentKey() {
        InstrumentKey newKey = new InstrumentKey();
        instrumentKeys.add(newKey);

        newKey.getShape().setWidth(DEFAULT_KEY_WIDTH);
        newKey.getShape().setHeight(DEFAULT_KEY_HEIGHT);

        if (selectedKeys.isEmpty()) {
            this.selectedKeys.add(newKey);
        } else {
            this.selectedKeys.set(0, newKey);
        }
        return newKey;
    }

    /**
     * To know if a position is in an InstrumentKey of the Instrument.
     *
     * @param pPosition position to test on the Instrument
     * @return The InstrumentKey that was pressed if an InstrumentKey was
     * pressed, otherwise null
     */
    private InstrumentKey keyClickedBy(Point pPosition) {

        InstrumentKey key;
        Iterator<InstrumentKey> it = instrumentKeys.descendingIterator();

        while (it.hasNext()) {

            key = it.next();

            if ((state == InstrumentState.PLAYING && key.isPlayable())) {
                if (key.isInside(pPosition)) {
                    return key;
                }
            } else if (state == InstrumentState.EDITING && key.isInside(pPosition)) {
                return key;
            }
        }

        return null;
    }

    /**
     * Presses a position on the Instrument.
     *
     * If an InstrumentKey is at the given position, that key is pressed and
     * true is returned, otherwise, false is returned.
     *
     * @param pPosition position to press on the Instrument
     * @return true if an InstrumentKey is at the given position, otherwise
     * false
     */
    public InstrumentKey pressInstrument(Point pPosition) {

        InstrumentKey iKey = keyClickedBy(pPosition);

        switch (state) {
            case EDITING:
                this.selectedKeys.clear();
                if (iKey != null) {
                    this.selectedKeys.add(iKey);
                }
                break;
            case PLAYING:
                this.selectedKeys.clear();
                if (iKey != null) {
                    this.selectedKeys.add(iKey);
                    iKey.press();
                }
                break;
        }

        return iKey;
    }

    public void selectedKeyWithUUID(UUID uuid) {
        for (InstrumentKey key : instrumentKeys) {
            if (key.getUuid().equals(uuid)) {
                selectedKeys.clear();
                selectedKeys.add(key);
                break;
            }
        }
    }

    /**
     * Move an InstrumentKey on the Instrument by the amount given in the Point
     * parameter.
     *
     * @param pIKey InstrumentKey to move
     * @param pDeltaPosition amount by which to move the InstrumentKey
     */
    public void moveKey(InstrumentKey pIKey, Point pDeltaPosition) {
        pIKey.move(pDeltaPosition);
    }

    // rechercherTouche(expression):touche
    public List<InstrumentKey> searchKey(String expression) {
        this.searchStrategy.setExpression(expression);
        return instrumentKeys.stream().filter(m -> m.isPlayable() && searchStrategy.filter(m)).collect(Collectors.toList());
    }

    public void setSearchStrategy(InstrumentKeySearchStrategy strategy) {
        if (strategy == null) {
            throw new NullPointerException("Null strategy");
        }
        this.searchStrategy = strategy;
    }

    public void releaseInstrument() {
        if (!selectedKeys.isEmpty()) {
            if (state == InstrumentState.PLAYING) {
                selectedKeys.forEach(m -> m.release());
                selectedKeys.clear();
            }
        }
    }

    public void moveSelectedKeyUpInLayers() {

        int currentIndex = instrumentKeys.indexOf(selectedKeys.get(0));
        int nextIndex = currentIndex + 1;

        if (currentIndex != -1 && nextIndex < instrumentKeys.size()) {
            InstrumentKey temp = instrumentKeys.get(currentIndex);
            instrumentKeys.set(currentIndex, instrumentKeys.get(nextIndex));
            instrumentKeys.set(nextIndex, temp);
        }
    }

    public void moveSelectedKeyDownInLayers() {

        int currentIndex = instrumentKeys.indexOf(selectedKeys.get(0));
        int previousIndex = currentIndex - 1;

        if (currentIndex != -1 && previousIndex >= 0) {
            InstrumentKey temp = instrumentKeys.get(currentIndex);
            instrumentKeys.set(currentIndex, instrumentKeys.get(previousIndex));
            instrumentKeys.set(previousIndex, temp);
        }
    }

    public InstrumentKey copySelectedKey() {
        if (!selectedKeys.isEmpty()) {
            selectedKeys.set(0, CopyUtils.copy(selectedKeys.get(0)));
            instrumentKeys.add(selectedKeys.get(0));
            selectedKeys.get(0).move(new Point(0.01, 0.01)); // Small translate to avoid overlap
        }

        return selectedKeys.get(0);
    }

    public void putSelectedKeyBack() {
        if (!selectedKeys.isEmpty()) {
            instrumentKeys.remove(selectedKeys.get(0));
            instrumentKeys.push(selectedKeys.get(0));
        }
    }

    public void putSelectedKeyFront() {
        if (!selectedKeys.isEmpty()) {
            instrumentKeys.remove(selectedKeys.get(0));
            instrumentKeys.add(selectedKeys.get(0));
        }
    }

    public void deleteSelectedKey() {
        instrumentKeys.remove(selectedKeys.get(0));
        selectedKeys.clear();
    }

    public Rectangle getInstrumentBounds() {
        double minX = 0;
        double minY = 0;
        double maxX = 0;
        double maxY = 0;

        Shape s;
        Point center;

        for (InstrumentKey key : instrumentKeys) {
            s = key.getShape();
            center = s.getCenter();
            minX = Math.min(center.getX() - s.getWidth() / 2, minX);
            minY = Math.min(center.getY() - s.getHeight() / 2, minY);
            maxX = Math.max(s.getWidth() / 2 + center.getX(), maxX);
            maxY = Math.max(s.getHeight() / 2 + center.getY(), maxY);
        }

        Rectangle r = new Rectangle(new Point[]{
            new Point(0, 0),
            new Point(maxX, 0),
            new Point(maxX, maxY),
            new Point(0, maxY)});

        return r;
    }

    public int getKeyCount() {
        return instrumentKeys.size();
    }
}

//+appuyerTouche(touche : Touche) : void
//+deplacerTouche(nomTouche : string, deltaPoint : Point) : void
