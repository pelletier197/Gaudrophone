/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.gaudrohero;

import gaudrophone.instrument.InstrumentKey;

/**
 *
 * @author sunny
 */
public class ScoreEvent {

    private final InstrumentKey instrumentKey;
    private final int pointsEarned;

    public ScoreEvent(InstrumentKey instrumentKey, int pointsEarned) {
        this.instrumentKey = instrumentKey;
        this.pointsEarned = pointsEarned;
    }

    public InstrumentKey getInstrumentKey() {
        return instrumentKey;
    }

    public int getPointsEarned() {
        return pointsEarned;
    }
}
