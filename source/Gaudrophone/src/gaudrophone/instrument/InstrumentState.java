/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument;

/**
 *
 * @author sunny
 */
public enum InstrumentState {
    PLAYING, // Once they are released, the keys get back to their normal state
    EDITING // In this state, the instrument keys stay on the "click" state even after they are released
}
