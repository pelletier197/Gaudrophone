/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound.generated;

/**
 *
 * @author sunny
 */
public enum GeneratedSoundInstrument {

    /**
     *
     */
    // Piano
    PIANO(0, GeneratedSoundManager.ANY_CHANNEL),
    ELECTRIC_PIANO(4, GeneratedSoundManager.ANY_CHANNEL),
    HARPSICHORD(6, GeneratedSoundManager.ANY_CHANNEL),
    // Chromatic Percussion
    VIBRAPHONE(11, GeneratedSoundManager.ANY_CHANNEL),
    XYLOPHONE(13, GeneratedSoundManager.ANY_CHANNEL),
    // Organ
    ORGAN(16, GeneratedSoundManager.ANY_CHANNEL),
    ACCORDION(21, GeneratedSoundManager.ANY_CHANNEL),
    HARMONICA(22, GeneratedSoundManager.ANY_CHANNEL),
    // Guitar
    NYLON_GUITAR(24, GeneratedSoundManager.ANY_CHANNEL),
    ACOUSTIC_GUITAR(25, GeneratedSoundManager.ANY_CHANNEL),
    CLEAN_GUITAR(27, GeneratedSoundManager.ANY_CHANNEL),
    MUTED_GUITAR(28, GeneratedSoundManager.ANY_CHANNEL),
    OVERDRIVE_GUITAR(29, GeneratedSoundManager.ANY_CHANNEL),
    DISTORTION_GUITAR(30, GeneratedSoundManager.ANY_CHANNEL),
    UKULELE(182, GeneratedSoundManager.ANY_CHANNEL),
    TWELVE_STRING_GUITAR(183, GeneratedSoundManager.ANY_CHANNEL),
    // Bass
    ACOUSTIC_BASE(32, GeneratedSoundManager.ANY_CHANNEL),
    FINGERED_BASE(33, GeneratedSoundManager.ANY_CHANNEL),
    PICKED_BASE(34, GeneratedSoundManager.ANY_CHANNEL),
    SLAP_BASE(36, GeneratedSoundManager.ANY_CHANNEL),
    // Strings
    VIOLIN(40, GeneratedSoundManager.ANY_CHANNEL),
    CELLO(42, GeneratedSoundManager.ANY_CHANNEL),
    CONTREBASS(43, GeneratedSoundManager.ANY_CHANNEL),
    // Brass
    TRUMPET(56, GeneratedSoundManager.ANY_CHANNEL),
    TROMBONE(57, GeneratedSoundManager.ANY_CHANNEL),
    TUBA(58, GeneratedSoundManager.ANY_CHANNEL),
    // Reed
    SOPRANO_SAXOPHONE(64, GeneratedSoundManager.ANY_CHANNEL),
    ALTO_SAXOPHONE(65, GeneratedSoundManager.ANY_CHANNEL),
    TENOR_SAXOPHONE(66, GeneratedSoundManager.ANY_CHANNEL),
    BARITONE_SAXOPHONE(67, GeneratedSoundManager.ANY_CHANNEL),
    CLARINET(71, GeneratedSoundManager.ANY_CHANNEL),
    // Pipe
    PICCOLO(72, GeneratedSoundManager.ANY_CHANNEL),
    FLUTE(73, GeneratedSoundManager.ANY_CHANNEL),
    PAN_FLUTE(75, GeneratedSoundManager.ANY_CHANNEL),
    OCARINA(79, GeneratedSoundManager.ANY_CHANNEL),
    MUTE_TRIANGLE(80, GeneratedSoundManager.ANY_CHANNEL),
    OPEN_TRIANGLE(81, GeneratedSoundManager.ANY_CHANNEL),
    // Synth Lead
    // Synth Pad
    // Synth Effects
    // Ethnic
    BANJO(105, GeneratedSoundManager.ANY_CHANNEL),
    // Percussive
    // Sound effects

    // Other
    DRUM(226, 9),
    SFX(234, 9); // Drums are played on channel 9

    private final int midiInstrument;
    private final int preferredChannel;

    public static GeneratedSoundInstrument getInstrumentFromOrdinal(int i) {
        for (GeneratedSoundInstrument inst : GeneratedSoundInstrument.values()) {
            if (inst.midiInstrument == i) {
                return inst;
            }
        }
        return null;
    }
    
    private GeneratedSoundInstrument(int midiInstrument, int preferredChannel) {
        this.midiInstrument = midiInstrument;
        this.preferredChannel = preferredChannel;
    }

    protected int getMidiInstrument() {
        return midiInstrument;
    }

    protected int getPreferredChannel() {
        return preferredChannel;
    }

}
