/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound.generated;

/**
 *
 * @author sunny
 */
public class GeneratedSoundException extends Exception {

    public GeneratedSoundException() {
    }

    public GeneratedSoundException(String message) {
        super(message);
    }

    public GeneratedSoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneratedSoundException(Throwable cause) {
        super(cause);
    }

    public GeneratedSoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
