/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound.generated;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;

/**
 *
 * @author sunny
 */
public class GeneratedSoundManager {

    /**
     * Used to ensure that the sound bank is there. This file was taken at
     * https://timtechsoftware.com/general-midi-soundfont-download/download
     */
    private static final String BANK_PATH = "/sound/soundbank.sf2";

    private static final int MAX_PRESS_VELOCITY = 127;

    public static final int ANY_CHANNEL = -1;
    public static final List<Integer> RESERVED_CHANNELS = Arrays.asList(new Integer[]{9});

    private Synthesizer synthesizer;
    private final MidiChannel[] midiChannels;
    private final Instrument[] instruments;

    private final Map<Instrument, ChannelInformation> instrumentChannel;
    private final List<ChannelInformation> channelsInfo;

    /**
     * This constructor is private to prevent multiple constructions
     *
     * @throws GeneratedSoundException
     */
    private GeneratedSoundManager() throws GeneratedSoundException {
        try {
            synthesizer = MidiSystem.getSynthesizer();
            synthesizer.open();

        } catch (MidiUnavailableException e) {
            throw new GeneratedSoundException("Can't open the channel");
        }

        this.midiChannels = synthesizer.getChannels();
        Soundbank bank = getDefaultSoundBank();
        synthesizer.loadAllInstruments(bank);

        this.instruments = synthesizer.getAvailableInstruments();

        synthesizer.loadAllInstruments(synthesizer.getDefaultSoundbank());

        this.instrumentChannel = new HashMap<>();
        this.channelsInfo = new ArrayList<>(midiChannels.length);
        createChannelInfo();
    }

    private Soundbank getDefaultSoundBank() throws GeneratedSoundException {

        // return synthesizer.getDefaultSoundbank();
        try {
            return MidiSystem.getSoundbank(new BufferedInputStream(getClass().getResourceAsStream(BANK_PATH)));
        } catch (IOException | InvalidMidiDataException ex) {
            throw new GeneratedSoundException(ex);
        }
    }

    /**
     * Plays the given note on the instrument with the given index, with a
     * specific volume.
     *
     * @param note The note to play, between 0 and 127, where middle C is 60
     * @param instrumentIndex The instrument index in the default sound bank
     * @param volume The volume to play the note, between 0 and 1
     * @param preferredChannel The preferred channel
     */
    public void playNote(int note, int instrumentIndex, double volume, int preferredChannel) {

        Instrument instrument = instruments[instrumentIndex];
        ChannelInformation channelInfo = instrumentChannel.get(instrument);
        if (channelInfo == null) {
            channelInfo = assignToChannel(instrument, preferredChannel);
        }
        channelInfo.getChannel().noteOn(note, (int) (volume * MAX_PRESS_VELOCITY));
    }

    public void stopNote(int note, int instrumentIndex) {
        Instrument instrument = instruments[instrumentIndex];
        ChannelInformation channelInfo = instrumentChannel.get(instrument);

        if (channelInfo != null) {
            channelInfo.getChannel().noteOff(note);
        }
    }

    /**
     * Will associate the given instrument to the channel in parameter
     *
     * @param channel
     * @param instrumentIndex
     * @return
     */
    private ChannelInformation assignToChannel(Instrument instrument, int preferredChannel) {

        ChannelInformation info = findUnusedChannel(preferredChannel);
        Instrument current = info.getCurrentInstrument();

        if (current != null) {
            instrumentChannel.remove(current);
        }

        // Switches to the new channel
        instrumentChannel.put(instrument, info);
        info.setCurrentInstrument(instrument);
        info.getChannel().programChange(instrument.getPatch().getProgram());

        return info;
    }

    private ChannelInformation findUnusedChannel(int preferredChannel) {
        if (preferredChannel == ANY_CHANNEL) {
            return findAnyUnusedChannel();
        }
        return channelsInfo.get(preferredChannel);
    }

    private ChannelInformation findAnyUnusedChannel() {
        // Gets the first unused and unreserved channel
        ChannelInformation info = channelsInfo.stream().filter(m -> m.getChannel() != null && !m.isUsed() && !m.isChannelReserved() && m.getCurrentInstrument() == null).findFirst().orElse(null);

        if (info != null) {
            return info;
        }

        // Returns the channel that was activated last
        return channelsInfo.stream().min((c1, c2) -> {
            if (c1.isChannelReserved() || c1.getChannel() == null) {
                return 1; // c1 is bigger than c2. This avoid using a reserved channel.
            }

            if (c2.isChannelReserved() || c2.getChannel() == null) {
                return -1;
            }

            return Long.compare(c1.getLastUsage(), c2.getLastUsage());
        }).get();
    }

    private void createChannelInfo() {
        int index = 0;
        for (MidiChannel chan : midiChannels) {
            ChannelInformation info = new ChannelInformation(chan, index);
            info.setIsChannelReserved(isReserved(index));
            channelsInfo.add(info);
            index++;
        }
    }

    private boolean isReserved(int channel) {
        return RESERVED_CHANNELS.contains(channel);
    }

    /**
     * The unique instance of this class
     */
    private static GeneratedSoundManager instance;

    public static GeneratedSoundManager getInstance() {
        if (instance == null) {
            try {
                instance = new GeneratedSoundManager();
            } catch (GeneratedSoundException ex) {
                ex.printStackTrace();
            }
        }
        return instance;
    }

    private class ChannelInformation {

        private int noteOnCount = 0;
        private final MidiChannel channel;
        private final int channelIndex;
        private long lastUsage;
        private boolean isChannelReserved;

        private Instrument currentInstrument;

        public ChannelInformation(MidiChannel channel, int channelIndex) {
            this.channel = channel;
            this.channelIndex = channelIndex;
        }

        public void noteOn() {
            noteOnCount++;
            lastUsage = System.currentTimeMillis();
        }

        public void noteOff() {
            if (noteOnCount >= 1) {
                noteOnCount--;
            }
        }

        public Instrument getCurrentInstrument() {
            return currentInstrument;
        }

        public void setCurrentInstrument(Instrument currentInstrument) {
            this.currentInstrument = currentInstrument;
        }

        public boolean isUsed() {
            return noteOnCount != 0;
        }

        public MidiChannel getChannel() {
            return channel;
        }

        public int getChannelIndex() {
            return channelIndex;
        }

        public long getLastUsage() {
            return lastUsage;
        }

        public boolean isChannelReserved() {
            return isChannelReserved;
        }

        public void setIsChannelReserved(boolean isChannelReserved) {
            this.isChannelReserved = isChannelReserved;
        }

    }
}
