/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound.file;

import gaudrophone.instrument.sound.Sound;
import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author sunny
 */
public class FileSound extends Sound {

    private static final float MAX_DECIBEL =  80;

    private String filePath;
    private transient Clip clip;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        String[] splitPath = filePath.split("\\.");
        if (splitPath.length > 1 && splitPath[splitPath.length - 1].equalsIgnoreCase("wav") && new File(filePath).exists()) {
            this.filePath = filePath;
        } else {
            throw new InvalidParameterException("Provided file path " + filePath + "was either invalid or the file was in a wrong format");
        }
    }

    @Override
    public void playSound() {
        File file = new File(filePath);
        if (clip == null || !clip.isRunning()) {
            try {
                AudioInputStream inputStream = AudioSystem.getAudioInputStream(file.toURI().toURL());
                clip = AudioSystem.getClip();
                clip.open(inputStream);

                FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                volume.setValue((float)  (-1 * (MAX_DECIBEL - MAX_DECIBEL * getVolume())));

                clip.start();
            } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex) {

            }
        }
    }

    @Override
    public void stopSound() {
    }

}
