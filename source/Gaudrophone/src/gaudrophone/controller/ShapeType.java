/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.controller;

/**
 *
 * @author sunny
 */
public enum ShapeType {
    ELLIPSE("Ellipse"),
    TRIANGLE("Triangle"),
    RECTANGLE("Rectangle"),
    DIAMOND("Losange"),
    TRAPEZOID("Trapèze"),
    PENTAGON("Pentagone"),
    HEXAGON("Hexagone"),
    OCTOGON("Octogone");
    
    private final String name;
    
    private ShapeType(String displayName){
        this.name = displayName;
    }
    
    public static ShapeType getShapeTypeByValue(String value){
        for (ShapeType shapeType : ShapeType.values()){
            if (shapeType.toString().equals(value)){
                return shapeType;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }



}
