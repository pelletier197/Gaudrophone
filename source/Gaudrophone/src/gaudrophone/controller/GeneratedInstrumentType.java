/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.controller;

/**
 *
 * @author sunny
 */
public enum GeneratedInstrumentType {
    // Piano
    PIANO("Piano"),
    ELECTRIC_PIANO("Piano électrique"),
    HARPSICHORD("Clavecin"),
    // Chromatic Percussion
    VIBRAPHONE("Vibraphone"),
    XYLOPHONE("Xylophone"),
    // Organ
    ORGAN("Orgue"),
    ACCORDION("Accordéon"),
    HARMONICA("Harmonica"),
    // Guitar
    NYLON_GUITAR("Guitare classique"),
    ACOUSTIC_GUITAR("Guitare acoustique"),
    CLEAN_GUITAR("Guitare (son propre)"),
    MUTED_GUITAR("Guitare (son muet)"),
    OVERDRIVE_GUITAR("Guitare (avec overdrive)"),
    DISTORTION_GUITAR("Guitare (avec distorsion"),
    UKULELE("Ukulele"),
    TWELVE_STRING_GUITAR("Guitare à 12 cordes"),
    // Bass
    ACOUSTIC_BASE("Base acoustique"),
    FINGERED_BASE("Base (doigté)"),
    PICKED_BASE("Base (pické)"),
    SLAP_BASE("Base (frappé)"),
    // Strings
    VIOLIN("Violon"),
    CELLO("Violoncelle"),
    CONTREBASS("Contrebasse"),
    // Brass
    TRUMPET("Trompette"),
    TROMBONE("Trombonne"),
    TUBA("Tuba"),
    // Reed
    SOPRANO_SAXOPHONE("Saxophone soprano"),
    ALTO_SAXOPHONE("Saxophone alto"),
    TENOR_SAXOPHONE("Saxophone Tenor"),
    BARITONE_SAXOPHONE("Saxophone baritone"),
    CLARINET("Clarinette"),
    // Pipe
    PICCOLO("Piccolo"),
    FLUTE("Flute"),
    PAN_FLUTE("Flute de Pan"),
    OCARINA("Ocarina"),
    MUTE_TRIANGLE("Triangle muet"),
    OPEN_TRIANGLE("Triangle ouvert"),
    // Synth Lead
    // Synth Pad
    // Synth Effects
    // Ethnic
    BANJO("Banjo"),
    // Percussive
    // Sound effects

    // Other
    DRUM("Percussions 1"),
    SFX("Percussions 2"); // Drums are played on channel 9
    
    private final String displayName;
    
    private GeneratedInstrumentType(String display){
        this.displayName = display;
    }

    @Override
    public String toString() {
        return displayName;
    }  
}
