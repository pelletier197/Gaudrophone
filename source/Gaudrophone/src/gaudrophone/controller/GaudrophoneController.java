/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.controller;

import gaudrophone.controller.DTO.InstrumentKeyDTO;
import gaudrophone.controller.helpers.DTOHelper;
import gaudrophone.drawing.InstrumentDrawer;
import gaudrophone.drawing.SwingUtils;
import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.InstrumentManager;
import gaudrophone.instrument.InstrumentState;
import gaudrophone.instrument.Metronome;
import gaudrophone.instrument.Note;
import gaudrophone.instrument.apparence.BorderType;
import gaudrophone.instrument.gaudrohero.GaudroHero;
import gaudrophone.instrument.gaudrohero.ScoreEvent;
import gaudrophone.instrument.geometry.Ellipse;
import gaudrophone.instrument.geometry.Shape;
import gaudrophone.instrument.geometry.shapes.Diamond;
import gaudrophone.instrument.geometry.shapes.Hexagon;
import gaudrophone.instrument.geometry.shapes.Octogon;
import gaudrophone.instrument.geometry.shapes.Pentagon;
import gaudrophone.instrument.geometry.shapes.Rectangle;
import gaudrophone.instrument.geometry.shapes.Trapezoid;
import gaudrophone.instrument.geometry.shapes.Triangle;
import gaudrophone.instrument.loops.LoopsManager;
import gaudrophone.instrument.music.Composition;
import gaudrophone.instrument.music.CompositionManager;
import gaudrophone.instrument.music.CompositionRepository;
import gaudrophone.instrument.music.MusicBox;
import gaudrophone.instrument.music.MusicalEventListener;
import gaudrophone.instrument.search.InstrumentKeyFrequencySearchStrategy;
import gaudrophone.instrument.search.InstrumentKeyNameSearchStrategy;
import gaudrophone.instrument.search.InstrumentKeyNoteSearchStrategy;
import gaudrophone.instrument.search.InstrumentKeyOctaveSearchStrategy;
import gaudrophone.instrument.search.InstrumentKeyPersistanceSeachStrategy;
import gaudrophone.instrument.search.InstrumentKeySearchStrategy;
import gaudrophone.instrument.sound.Sound;
import gaudrophone.instrument.sound.file.FileSound;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.sound.generated.GeneratedSoundInstrument;
import gaudrophone.instrument.sound.generated.GeneratedSoundManager;
import gaudrophone.instrument.utils.CopyUtils;
import gaudrophone.instrument.utils.StringUtils;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JPanel;
import panel.CustomDrawingPanel;

/**
 *
 * @author sunny
 */
public class GaudrophoneController {

    private Instrument instrument;
    private InstrumentKey lastSelectedKey;
    private final InstrumentManager instrumentManager;
    private final CompositionManager compositionManager;
    private InstrumentDrawer drawer;
    private InstrumentState state;
    private CustomDrawingPanel drawingPanel;
    private LoopsManager loopsManager;
    private boolean isLoopingHandlingActive;
    private MusicBox musicBox;

    private final Metronome metronome;
    private boolean isProgressBarUpdatable;

    private GaudroHero hero;

    public GaudrophoneController() {
        GeneratedSoundManager.getInstance(); // Just to warm up the sound manager
        this.drawer = new InstrumentDrawer();
        this.instrumentManager = InstrumentManager.getInstance();
        this.compositionManager = CompositionManager.getInstance();
        this.state = InstrumentState.PLAYING;
        metronome = new Metronome();
        setIsLoopingHandlingActive(false);
    }

    public boolean getIsLoopingHandlingActive() {
        return isLoopingHandlingActive;
    }

    public void setIsLoopingHandlingActive(boolean value) {
        isLoopingHandlingActive = value;

        if (loopsManager != null && !value) {
            loopsManager.stopAll();
        }

        if (value && instrument != null) {
            loopsManager = new LoopsManager(instrument, getMusicalEventListener());
        }
    }

    public static String[] getAvailableNotes() {
        return Arrays.asList(Note.values()).stream().map(m -> m.toString()).collect(Collectors.toList()).toArray(new String[Note.values().length]);
    }

    public static String[] getAvailableBorders() {
        return Arrays.asList(BorderType.values()).stream().map(m -> m.toString()).collect(Collectors.toList()).toArray(new String[BorderType.values().length]);
    }

    public Collection<String> getAvailableCustomInstruments() {
        return instrumentManager.getCustomInstrumentsNames();
    }

    public Collection<String> getAvailableTemplatesNames() {
        return instrumentManager.getTemplateStrategyNames();
    }

    public Collection<String> getAvailableCompositions() {
        return compositionManager.getCompositionNames();
    }

    public void switchToEditMode() {
        this.state = InstrumentState.EDITING;

        if (instrument != null) {
            instrument.setState(state);
        }
    }

    public void switchToPlayingMode() {
        this.state = InstrumentState.PLAYING;

        if (this.instrument != null) {
            loopsManager = new LoopsManager(instrument, getMusicalEventListener());
        }

        if (instrument != null) {
            instrument.setState(state);
        }
    }

    public void setInstrument(InstrumentType type, String name) {
        switch (type) {
            case CUSTOM:
                this.instrument = instrumentManager.loadCustomInstrumentFromName(name);
                break;
            case NEW_INSTRUMENT:
                this.instrument = new Instrument();
                break;
            case TEMPLATE:
                this.instrument = instrumentManager.getInstrumentFromStrategyName(name);
                break;
        }
        this.instrument.setState(state);
        this.drawer.setInstrument(instrument);
        this.drawer.forceRedraw();
        this.lastSelectedKey = instrument.getSelectedKey();
        setIsLoopingHandlingActive(isLoopingHandlingActive);
    }

    public void setDrawingPanel(CustomDrawingPanel panel) {
        this.drawer.setCustomDrawingPanel(panel);
        this.drawingPanel = panel;
    }

    public String handleKeyPressed(KeyEvent event) {
        if (state.equals(InstrumentState.PLAYING) && isValidLoopKey(event.getKeyChar())) {
            return loopsManager.loopKeyPressed(Character.getNumericValue(event.getKeyChar())).getName();
        }
        return null;
    }

    private boolean isValidLoopKey(char c) {
        int val = Character.getNumericValue(c);
        return val >= 1 && val <= 9;
    }

    public void setInstrumentKeySoundFile(String file) {
        if (lastSelectedKey != null) {

            Sound s = lastSelectedKey.getSound();
            s.stop();

            if (!(s instanceof FileSound)) {
                lastSelectedKey.setSound(new FileSound());
                CopyUtils.copyProperties(s, lastSelectedKey.getSound());
            }

            FileSound sound = (FileSound) lastSelectedKey.getSound();
            sound.setFilePath(file);
        }
    }

    public void setNoteAndOctave(String note, int octave) {
        System.out.println("Note :" + note + " Octave : " + octave);
        if (lastSelectedKey != null) {

            Note n = Note.fromString(note);
            Sound s = lastSelectedKey.getSound();

            s.setNote(n);
            s.setOctave(octave);
            drawer.forceRedraw();
        }
    }

    public void setFrequency(double freq) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getSound().setFrequency(freq);
            drawer.forceRedraw();
        }
    }

    public void setVolume(double volume) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getSound().setVolume(volume);
            drawer.forceRedraw();
        }

    }

    public void setGeneratedInstrumentSoundType(int generatedInstrumentTypeOrdinal) {

        if (lastSelectedKey != null) {
            GeneratedSoundInstrument inst = GeneratedSoundInstrument.values()[generatedInstrumentTypeOrdinal];
            Sound s = lastSelectedKey.getSound();
            s.stop();

            lastSelectedKey.setSound(new GeneratedSound(s.getNote(), s.getOctave(), inst));
            CopyUtils.copyProperties(s, lastSelectedKey.getSound()); // Copy the properties from the old sound
        }
    }

    public void testInstrumentPress() {
        if (lastSelectedKey != null) {
            lastSelectedKey.press();
        }
    }

    public void testInstrumentRelease() {
        if (lastSelectedKey != null) {
            lastSelectedKey.release();
        }
    }

    public void handlePress(Point point) {
        if ((hero == null && instrument != null) || (hero != null && hero.isPlaying())) {
            lastSelectedKey = instrument.pressInstrument(asRelative(point));
            handleInstrumentKeyPressed(lastSelectedKey);
            drawer.forceRedraw();

            if (hero != null && hero.isPlaying() && lastSelectedKey != null) {
                hero.pressKey(lastSelectedKey);
            }
        }

    }

    public void handleRelease() {
        if ((hero == null && instrument != null) || (hero != null && hero.isPlaying())) {
            instrument.releaseInstrument();
            handleInstrumentKeyRelease(lastSelectedKey);
            drawer.forceRedraw();
        }
    }

    public InstrumentKeyDTO getSelectedKey() {

        return lastSelectedKey == null ? null : DTOHelper.InstrumentKeyToDTO(lastSelectedKey);
    }

    public List<InstrumentKeyDTO> searchInstrumentKey(String search, SearchType searchType) {
        instrument.setSearchStrategy(getSearchStrategy(searchType));
        return instrument.searchKey(search).stream().map(m -> DTOHelper.InstrumentKeyToDTO(m)).collect(Collectors.toList());
    }

    private InstrumentKeySearchStrategy getSearchStrategy(SearchType type) {
        switch (type) {
            case BY_FREQUENCY:
                return new InstrumentKeyFrequencySearchStrategy();
            case BY_NAME:
                return new InstrumentKeyNameSearchStrategy();
            case BY_OCTAVE:
                return new InstrumentKeyOctaveSearchStrategy();
            case BY_PERSISTANCE:
                return new InstrumentKeyPersistanceSeachStrategy();
            case BY_NOTE:
                return new InstrumentKeyNoteSearchStrategy();
        }
        return null;
    }

    public InstrumentKeyDTO createNewInstrumentKey() {
        InstrumentKey key = instrument.addInstrumentKey();
        drawer.forceRedraw();
        this.lastSelectedKey = key;
        return DTOHelper.InstrumentKeyToDTO(key);
    }

    private gaudrophone.instrument.geometry.Point asRelative(Point pt) {
        return SwingUtils.pointToRelative(pt, drawingPanel.getWidth(), drawingPanel.getHeight());
    }

    public void setDisplayNameEnabled(boolean selected) {
        if (instrument.getState() == InstrumentState.EDITING && lastSelectedKey != null) {
            lastSelectedKey.setNameDisplayed(selected);
            drawer.forceRedraw();
        } else if (instrument.getState() == InstrumentState.PLAYING) {
            for (InstrumentKey key : instrument.getInstrumentKeys()) {
                key.setNameDisplayed(selected);
            }
            drawer.forceRedraw();

        }
    }

    public void setDisplayVolumeEnabled(boolean selected) {
        if (instrument.getState() == InstrumentState.EDITING && lastSelectedKey != null) {
            lastSelectedKey.setVolumeDisplayed(selected);
            drawer.forceRedraw();
        } else if (instrument.getState() == InstrumentState.PLAYING) {
            for (InstrumentKey key : instrument.getInstrumentKeys()) {
                key.setVolumeDisplayed(selected);
            }
            drawer.forceRedraw();
        }
    }

    public void setDisplayPersistanceEnabled(boolean selected) {
        if (instrument.getState() == InstrumentState.EDITING && lastSelectedKey != null) {
            lastSelectedKey.setPersistanceDisplayed(selected);
            drawer.forceRedraw();
        } else if (instrument.getState() == InstrumentState.PLAYING) {
            for (InstrumentKey key : instrument.getInstrumentKeys()) {
                key.setPersistanceDisplayed(selected);
            }
            drawer.forceRedraw();

        }

    }

    public void setDisplayNoteAndOctaveEnable(boolean selected) {
        if (instrument.getState() == InstrumentState.EDITING && lastSelectedKey != null) {
            lastSelectedKey.setNoteAndOctaveDisplayed(selected);
            drawer.forceRedraw();
        } else if (instrument.getState() == InstrumentState.PLAYING) {
            for (InstrumentKey key : instrument.getInstrumentKeys()) {
                key.setNoteAndOctaveDisplayed(selected);
            }
            drawer.forceRedraw();

        }
    }

    public void setDisplayFrequencyEnable(boolean selected) {
        if (instrument.getState() == InstrumentState.EDITING && lastSelectedKey != null) {
            lastSelectedKey.setFrequencyDisplayed(selected);
            drawer.forceRedraw();
        } else if (instrument.getState() == InstrumentState.PLAYING) {
            for (InstrumentKey key : instrument.getInstrumentKeys()) {
                key.setFrequencyDisplayed(selected);
            }
            drawer.forceRedraw();

        }
    }

    public void setInstrumentKeyImageFile(File selectedFile) {
        if (lastSelectedKey != null) {
            try {
                lastSelectedKey.setImage(selectedFile);
                drawer.forceRedraw();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void setKeyPersistance(long d) {
        if (lastSelectedKey != null) {
            lastSelectedKey.setPersistance(d);
            drawer.forceRedraw();
        }
    }

    public void setKeyName(String text) {
        if (lastSelectedKey != null) {
            lastSelectedKey.setName(text);
            drawer.forceRedraw();
        }
    }

    public void setKeyHeight(double d) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getShape().setHeight(d);
            drawer.forceRedraw();
        }
    }

    public void setKeyWidth(double d) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getShape().setWidth(d);
            drawer.forceRedraw();
        }
    }

    public void setKeyPosX(double d) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getShape().setPosition(new gaudrophone.instrument.geometry.Point(d, lastSelectedKey.getShape().getCenter().getY()));
            drawer.forceRedraw();
        }
    }

    public void setKeyPosY(double d) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getShape().setPosition(new gaudrophone.instrument.geometry.Point(lastSelectedKey.getShape().getCenter().getX(), d));
            drawer.forceRedraw();
        }
    }

    public void setKeyShapeType(ShapeType shapeType) {
        if (lastSelectedKey != null) {
            Shape currentShape = lastSelectedKey.getShape();

            double height = currentShape.getHeight();
            double width = currentShape.getWidth();
            gaudrophone.instrument.geometry.Point center = currentShape.getCenter();

            Shape newShape = null;
            switch (shapeType) {
                case ELLIPSE:
                    newShape = new Ellipse(height, width, center);
                    break;
                case DIAMOND:
                    newShape = new Diamond();
                    break;
                case HEXAGON:
                    newShape = new Hexagon();
                    break;
                case OCTOGON:
                    newShape = new Octogon();
                    break;
                case PENTAGON:
                    newShape = new Pentagon();
                    break;
                case RECTANGLE:
                    newShape = new Rectangle();
                    break;
                case TRAPEZOID:
                    newShape = new Trapezoid();
                    break;
                case TRIANGLE:
                    newShape = new Triangle();
                    break;
                default:
                    throw new UnsupportedOperationException("ShapeType " + shapeType.toString() + " construction is not supported yet");
            }

            newShape.setWidth(width);
            newShape.setHeight(height);
            newShape.setPosition(center);

            lastSelectedKey.setShape(newShape);
            drawer.forceRedraw();
        }
    }

    public void setKeyBorderType(String string) {
        if (lastSelectedKey != null) {
            BorderType type = BorderType.fromString(string);
            lastSelectedKey.getBorder().setType(type);
            drawer.forceRedraw();
        }
    }

    public void setKeyBorderWidth(int width) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getBorder().setWidth(width);
            drawer.forceRedraw();
        }
    }

    public void setKeyBackgroundColor(Color selectedColor) {
        if (lastSelectedKey != null) {
            lastSelectedKey.setBackgroundColor(SwingUtils.toInstrumentColor(selectedColor));
            drawer.forceRedraw();
        }
    }

    public void makeKeyLayerUp() {
        if (lastSelectedKey != null) {
            instrument.moveSelectedKeyUpInLayers();
            drawer.forceRedraw();
        }
    }

    public void makeKeyLayerDown() {
        if (lastSelectedKey != null) {
            instrument.moveSelectedKeyDownInLayers();
            drawer.forceRedraw();
        }
    }

    public void dragKeyOnPanel(Point point) {
        if (lastSelectedKey != null) {
            lastSelectedKey.getShape().setPosition(asRelative(point));
            drawer.forceRedraw();
        }
    }

    public InstrumentKeyDTO copySelectedKey() {

        if (lastSelectedKey != null) {
            InstrumentKey key = instrument.copySelectedKey();
            lastSelectedKey = key;
            drawer.forceRedraw();
            return DTOHelper.InstrumentKeyToDTO(key);
        }

        return null;
    }

    public void putSelectedKeyBack() {
        if (lastSelectedKey != null) {
            instrument.putSelectedKeyBack();
            drawer.forceRedraw();
        }
    }

    public void putSelectedKeyFront() {
        if (lastSelectedKey != null) {
            instrument.putSelectedKeyFront();
            drawer.forceRedraw();
        }
    }

    public void deleteSelectedKey() {
        if (lastSelectedKey != null) {
            instrument.deleteSelectedKey();
            drawer.forceRedraw();
        }
    }

    public void selectKeyForUUID(UUID uuid) {
        if (instrument != null) {
            instrument.selectedKeyWithUUID(uuid);
            lastSelectedKey = instrument.getSelectedKey();
            drawer.forceRedraw();
        }
    }

    public void setInstrumentNameAndSave(String text) {

        // Notifies the manager to update the instruments name
        instrument.setName(text);
        instrumentManager.saveCustomInstrument(instrument);
    }

    public boolean deleteInstrumentFromName(String name) {
        return instrumentManager.deleteInstrumentFromName(name);
    }

    public String getInstrumentName() {
        return instrument.getName();
    }

    public void setKeyPlayable(boolean selected) {
        if (lastSelectedKey != null) {
            lastSelectedKey.setPlayable(selected);
            drawer.forceRedraw();
        }
    }

    public void setMetronomePlaying(boolean playing) {
        if (playing) {
            metronome.open();
        } else {
            metronome.close();
        }
    }

    public void setMetronomeBPM(int bpm) {
        metronome.setBPM(bpm);
    }

    public void handleStrumming(Point point) {

        if (hero == null || (hero != null && hero.isPlaying())) {
            InstrumentKey last = instrument.getSelectedKey();
            InstrumentKey newKey = instrument.pressInstrument(asRelative(point));

            if (last != newKey) {
                if (last != null) {
                    last.release();
                    handleInstrumentKeyRelease(last);
                }
                lastSelectedKey = newKey;
                handleInstrumentKeyPressed(newKey);
                drawer.forceRedraw();
            }
            if (newKey != null && hero != null && hero.isPlaying() && hero.isPlaying()) {
                hero.pressKey(newKey);
            }
        }
    }

    public void setProportionalResize(boolean selected) {
        drawer.setProportionalResize(selected);
    }

    public boolean isProportionalResize() {
        return drawer.isProportionalResize();
    }

    private void handleInstrumentKeyPressed(InstrumentKey newKey) {

        if (loopsManager != null && newKey != null) {
            loopsManager.instrumentKeyPressed(newKey);
        }
    }

    private void handleInstrumentKeyRelease(InstrumentKey newKey) {

        if (loopsManager != null && newKey != null) {
            loopsManager.instrumentKeyReleased(newKey);
        }
    }

    private MusicalEventListener getMusicalEventListener() {
        return new MusicalEventListener() {
            @Override
            public void onKeyPressed(InstrumentKey key) {
                drawer.forceRedraw();
            }

            @Override
            public void onKeyReleased(InstrumentKey key) {
                drawer.forceRedraw();
            }
        };
    }

    public List<String> searchComposition(String text) {
        Collection<String> compositions = getAvailableCompositions();
        List<String> result = compositions.stream().filter(m -> (m == null ? text == null : m.contains(text))).collect(Collectors.toList());
        return result;
    }

    public String loadCompositionFromFile(String path) throws Exception {
        CompositionRepository repository = new CompositionRepository();
        Composition compo = repository.parseFile(path);
        compositionManager.saveComposition(compo);
        return compo.getName();
    }

    public void setComposition(String name) {
        musicBox = new MusicBox(instrument);
        musicBox.setComposition(compositionManager.getCompositionFromName(name));
        musicBox.setMusicEventListener(getMusicalEventListener());
    }

    public void stopComposition() {
        musicBox.stop();
    }

    public void playCompositionAt(double time) {
        musicBox.stop();
        musicBox.startAt(time);
    }
    
    public void setCompositionAt(double time) {
        musicBox.stop();
        musicBox.setAt(time);
    }

    public void resumeComposition() {
        musicBox.resume();
    }

    public boolean isMusicBoxPlaying() {
        return musicBox.isPlaying();
    }

    public String getMusicBoxFormatedDuration() {
        return StringUtils.getFormatedDuration(musicBox.getDuration());
    }

    public String getCurrentMusicBoxTime() {
        return StringUtils.getFormatedDuration(musicBox.getCurrentTime());
    }

    public double getCurrentMusicBoxCompletionPercentage() {
        return musicBox.getCompletionPercentage();
    }

    public void setProgressBarUpdatable(boolean value) {
        isProgressBarUpdatable = value;
    }

    public boolean getIsProgressBarUpdatable() {
        return isProgressBarUpdatable;
    }

    public String getCompositionScore() {
        if (musicBox != null) {
            return musicBox.getComposition().getScore();
        }
        return "";
    }

    public void dissociate() {
        if (instrument != null) {
            instrument.releaseInstrument();
        }
        if (loopsManager != null) {
            loopsManager.stopAll();
        }
        if (metronome != null) {
            metronome.close();
        }

        if (musicBox != null) {
            musicBox.stop();
        }
        drawer = null;
    }


    public void deleteCompositionFromName(String name) {
        compositionManager.deleteCompositionFromName(name);
    }
    
    public void makeGaudroHero() {
        hero = new GaudroHero();
        hero.setMusicBox(musicBox);
        hero.setMusicalEventListener(getMusicalEventListener());
    }

    public boolean isGaudroHeroPlaying() {
        return hero.isPlaying();
    }

    public void pauseGaudroHero() {
        hero.pause();
    }

    public void resumeGaudroHero() {
        hero.play();
    }

    public void setGaudroHeroSpeed(double fraction) {
        hero.setSpeed(fraction);
    }

    public double getGaudroHeroSpeed() {
        return hero.getSpeed();
    }

    public String getGaudroHeroHistory() {
        StringBuilder builder = new StringBuilder("<html>");

        hero.getScoreHistory().forEach((e) -> {
            builder.append(e.getInstrumentKey().toString()).append(" - ").append(e.getPointsEarned()).append(" points <br/>");
        });
        
        builder.append("</html>");

        return builder.toString();
    }

    public int getGaudroHeroStreakCount() {
        return hero.getStreakCount();
    }

    public int getGaudroHeroScoreMultiplier() {
        return hero.getScoreMultiplier();
    }

    public int getGaudroHeroPoints() {
        return hero.getPoints();
    }

    public int getGaudroHeroSuccessCount() {
        return hero.getSuccessCount();
    }

    public int getGaudroHeroFailedCount() {
        return hero.getFailedCount();
    }

    public String getGaudroHeroScore() {
        return String.format("%.1f", hero.getGaudroScore() * 100) + "%";

    }
}
