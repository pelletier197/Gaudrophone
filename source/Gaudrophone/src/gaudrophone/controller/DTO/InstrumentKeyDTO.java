/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.controller.DTO;

import java.awt.Color;
import gaudrophone.instrument.geometry.Point;
import java.util.UUID;

/**
 *
 * @author sunny
 */
public class InstrumentKeyDTO {

    private Point relativePosition;
    private ShapeDTO shape;
    private SoundDTO sound;
    private BorderDTO border;
    private Color color;
    private String name;
    private String imageFile;

    private String display;
    private UUID uuid;

    private boolean nameDisplayed;
    private boolean persistanceDisplayed;
    private boolean noteAndOctaveDisplayed;
    private boolean frequencyDisplayed;
    private boolean volumeDisplayed;

    private boolean playable;

    public BorderDTO getBorder() {
        return border;
    }

    public void setBorder(BorderDTO border) {
        this.border = border;
    }

    public ShapeDTO getShape() {
        return shape;
    }

    public void setShape(ShapeDTO shape) {
        this.shape = shape;
    }

    public SoundDTO getSound() {
        return sound;
    }

    public void setSound(SoundDTO sound) {
        this.sound = sound;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getRelativePosition() {
        return this.relativePosition;
    }

    public void setRelativePosition(Point relativePosition) {
        this.relativePosition = relativePosition;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return display;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public boolean isFrequencyDisplayed() {
        return frequencyDisplayed;
    }

    public void setFrequencyDisplayed(boolean frequencyDisplayed) {
        this.frequencyDisplayed = frequencyDisplayed;
    }

    public boolean isNameDisplayed() {
        return nameDisplayed;
    }

    public void setNameDisplayed(boolean nameDisplayed) {
        this.nameDisplayed = nameDisplayed;
    }

    public boolean isPersistanceDisplayed() {
        return persistanceDisplayed;
    }

    public void setPersistanceDisplayed(boolean persistanceDisplayed) {
        this.persistanceDisplayed = persistanceDisplayed;
    }

    public boolean isNoteAndOctaveDisplayed() {
        return noteAndOctaveDisplayed;
    }

    public void setNoteAndOctaveDisplayed(boolean noteAndOctaveDisplayed) {
        this.noteAndOctaveDisplayed = noteAndOctaveDisplayed;
    }

    public boolean isVolumeDisplayed() {
        return volumeDisplayed;
    }

    public void setVolumeDisplayed(boolean volumeDisplayed) {
        this.volumeDisplayed = volumeDisplayed;
    }

    public boolean isPlayable() {
        return playable;
    }

    public void setPlayable(boolean playable) {
        this.playable = playable;
    }

}
