/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.controller;

/**
 *
 * @author sunny
 */
public enum SearchType {
    BY_NAME("Nom"),
    BY_FREQUENCY("Fréquence"),
    BY_OCTAVE("Octave"),
    BY_PERSISTANCE("Persistance"),
    BY_NOTE("Note");

    private final String display;

    private SearchType(String displayName) {
        this.display = displayName;
    }

    @Override
    public String toString() {
        return display;
    }

}
