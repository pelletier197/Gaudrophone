/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.view;

/**
 *
 * @author sunny
 */
public interface ScreenController {
    
    void showView();
    void setScreen(Screen screen, SetUpHelper helper);
}
