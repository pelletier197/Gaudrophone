/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.view;

import com.jtattoo.plaf.graphite.GraphiteLookAndFeel;
import gaudrophone.view.views.ChooseComposition;
import gaudrophone.view.views.ChooseInstrument;
import gaudrophone.view.views.ManageInstruments;
import gaudrophone.view.views.CreateInstrument;
import gaudrophone.view.views.Credits;
import gaudrophone.view.views.GaudroHero;
import gaudrophone.view.views.ListenComposition;
import gaudrophone.view.views.Menu;
import gaudrophone.view.views.Practice;
import gaudrophone.view.views.Record;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 *
 * @author sunny
 */
public class SwingScreenController extends JFrame implements ScreenController {

    private JPanel mainFrame;
    private CardLayout cards;
    private Map<Screen, ControlledScreen> loadedScreens;
    private ControlledScreen current;

    public SwingScreenController() {
        cards = new CardLayout();
        mainFrame = new JPanel(cards);
        loadedScreens = new HashMap<>();
        setStyle();
        setContentPane(mainFrame);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setScreen(Screen.values()[0], null);

    }

    @Override
    public void setScreen(Screen screen, SetUpHelper helper) {

        if (current != null) {
            current.removedFromScreen();
        }

        ControlledScreen cs;

        if ((cs = loadedScreens.get(screen)) == null) {
            cs = getPanel(screen);
            cs.setScreenController(this);
            mainFrame.add((Component) cs, screen.name());
            loadedScreens.put(screen, cs);
        }

        cards.show(mainFrame, screen.name());
        current = cs;
        cs.displayedToScreen(helper);

    }

    private ControlledScreen getPanel(Screen screen) {
        ControlledScreen panel = null;
        switch (screen) {
            case MAIN_MENU:
                panel = new Menu();
                break;
            case CREDITS:
                panel = new Credits();
                break;
            case CREATE_INSTRUMENT:
                panel = new CreateInstrument();
                break;
            case MANAGE_INSTRUMENT:
                panel = new ManageInstruments();
                break;
            case GAUDRO_HERO:
                panel = new GaudroHero();
                break;
            case RECORD:
                panel = new Record();
                break;
            case CHOOSE_COMPOSITION:
                panel = new ChooseComposition();
                break;
            case CHOOSE_INSTRUMENT:
                panel = new ChooseInstrument();
                break;
            case LISTEN_COMPOSITION:
                panel = new ListenComposition();
                break;
            case PRACTICE:
                panel = new Practice();
                break;
            default:
                throw new IllegalArgumentException("This screen is not supported");
        }
        return panel;
    }

    private void setStyle() {
        try {
            UIManager.setLookAndFeel(GraphiteLookAndFeel.class.getName());

        } catch (Exception e) {

        }

    }

    @Override
    public void showView() {
        pack();
        setVisible(true);
    }

}
