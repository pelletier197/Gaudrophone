/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.view;

/**
 *
 * @author sunny
 */
public interface ControlledScreen {
    
    public void displayedToScreen(SetUpHelper helper);
    public void setScreenController(ScreenController controller);
    public void removedFromScreen();
}
