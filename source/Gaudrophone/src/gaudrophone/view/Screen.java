/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.view;

/**
 *
 * @author sunny
 */
public enum Screen {
    MAIN_MENU,
    CREDITS,
    CREATE_INSTRUMENT,
    MANAGE_INSTRUMENT,
    RECORD,
    GAUDRO_HERO,
    CHOOSE_INSTRUMENT,
    LISTEN_COMPOSITION,
    PRACTICE,
    CHOOSE_COMPOSITION
    
}
