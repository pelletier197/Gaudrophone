/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone;

import gaudrophone.controller.InstrumentType;
import gaudrophone.instrument.music.CompositionManager;
import gaudrophone.instrument.music.CompositionRepository;
import gaudrophone.instrument.sound.generated.GeneratedSoundException;
import gaudrophone.instrument.template.PianoTemplateStrategy;
import gaudrophone.view.Screen;
import gaudrophone.view.SetUpHelper;
import gaudrophone.view.SwingScreenController;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.JFrame;

/**
 *
 * @author sunny
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws javax.sound.midi.MidiUnavailableException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws MidiUnavailableException, InterruptedException, GeneratedSoundException, Exception {

        SwingScreenController contr = new SwingScreenController();
        contr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contr.setScreen(Screen.MAIN_MENU, null);
        contr.showView();

    }

}
