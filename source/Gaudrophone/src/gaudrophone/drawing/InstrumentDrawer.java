/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.drawing;

import com.sun.javafx.font.Metrics;
import gaudrophone.instrument.InstrumentState;
import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.apparence.Border;
import gaudrophone.instrument.apparence.Color;
import gaudrophone.instrument.geometry.Point;
import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import panel.CustomDrawer;
import panel.CustomDrawingPanel;

/**
 *
 * @author sunny
 */
public class InstrumentDrawer implements CustomDrawer {

    private static final java.awt.Color EDITED_KEY_COLOR = new java.awt.Color(0xED0000);//Couleur rouge pour la touche en édition
    private static final java.awt.Color KEY_BORDER_COLOR = java.awt.Color.BLACK;
    private static final int TRANSPARENCY_EDIT = 65;

    private static final java.awt.Color DEMO_PRESSED_COLOR = new java.awt.Color(0xED0000);
    private static final int DARK_COLOR_TREESHOLD = 20;

    private static final int FONT_SIZE = 12;
    private static final Font TEXT_FONT = new Font(Font.DIALOG, Font.BOLD, FONT_SIZE);
    private static final int TEXT_PADDING_Y = 5;
    private static final java.awt.Color TEXT_COLOR = java.awt.Color.BLACK;

    private CustomDrawingPanel panel;
    private Instrument instrument;

    private int lastWidth;
    private int lastHeight;

    private boolean autisticResize = true;

    public void setCustomDrawingPanel(CustomDrawingPanel panel) {
        panel.setCustomDrawer(this);
        this.panel = panel;
        this.lastWidth = panel.getWidth();
        this.lastHeight = panel.getHeight();
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
        this.lastHeight = 0;
        this.lastWidth = 0;
    }

    public void forceRedraw() {
        this.panel.repaint(); // Repaint all the pannel
    }

    /**
     * Handles the drawing of the instrument
     *
     * @param graphics
     */
    @Override
    public void draw(Graphics2D graphics) {

        if (graphics.getFont() != TEXT_FONT) {
            graphics.setFont(TEXT_FONT);
        }

        if (autisticResize) {
            if (lastHeight == 0 || lastWidth == 0) {
                lastHeight = panel.getHeight();
                lastWidth = panel.getWidth();
            } else if (panel.getWidth() != lastWidth || panel.getHeight() != lastHeight) {

                gaudrophone.instrument.geometry.shapes.Rectangle bounds = instrument.getInstrumentBounds();

                // Still fits in screen, so just replace the components
                if (instrumentFitsInScreen(bounds)) {
                    replaceComponentsKeepAspect();
                    // Same visual Height and width
                    resizeComponentsKeepAspect();
                } else {
                    resizeComponentsKeepRatio(bounds);
                }
            }

            lastWidth = panel.getWidth();
            lastHeight = panel.getHeight();

        }

        this.instrument.getInstrumentKeys().forEach((instrumentKey) -> {

            java.awt.Shape shape = SwingUtils.generateShapeFromRelative(instrumentKey.getShape(), panel.getWidth(), panel.getHeight());

            graphics.setClip(null);

            // Fills the center of the shape
            graphics.setColor(getFillColor(instrumentKey));
            graphics.fill(shape);

            // Writes down the image
            BufferedImage image = getKeyImage(instrumentKey);
            if (image != null) {
                Rectangle bounds = shape.getBounds();
                graphics.setClip(shape);
                graphics.drawImage(image, (int) bounds.getMinX(), (int) bounds.getMinY(), (int) bounds.getWidth(), (int) bounds.getHeight(), null);
            }

            // Fills the border
            if (instrumentKey.getBorder().getWidth() > 0) {
                graphics.setColor(KEY_BORDER_COLOR);
                graphics.setStroke(getStroke(instrumentKey.getBorder()));
                graphics.draw(shape);
            }
            writeKeyText(instrumentKey, graphics, shape);
        });
    }

    private BufferedImage getKeyImage(InstrumentKey key) {
        BufferedImage image = key.getImage();

        if (image == null) {
            return null;
        }

        if (!key.isPressed()) {
            return image;
        }

        // darker image
        RescaleOp op = new RescaleOp(.8f, 0, null);
        return op.filter(image, null);
    }

    private java.awt.Color getFillColor(InstrumentKey key) {
        if (instrument.getState() == InstrumentState.EDITING) {
            if (key == instrument.getSelectedKey()) {
                return EDITED_KEY_COLOR;
            } else {
                Color c = key.getBackgroundColor();
                return new java.awt.Color(c.getRed(), c.getGreen(), c.getBlue(), TRANSPARENCY_EDIT);
            }
        } else if (key.isPressed()) {
            // https://stackoverflow.com/questions/9780632/how-do-i-determine-if-a-color-is-closer-to-white-or-black
            Color c = key.getBackgroundColor();
            double luminosity = 0.2126 * c.getRed() + 0.7152 * c.getGreen() + 0.0722 * c.getBlue();

            // Will make the key brighter if it is too dark already
            if (luminosity <= DARK_COLOR_TREESHOLD) {
                c = new Color(c.getAlpha(), 128 - c.getRed(), 128 - c.getGreen(), 128 - c.getBlue());
                java.awt.Color col = SwingUtils.fromInstrumentColor(c);
                return col;
            }

            return SwingUtils.fromInstrumentColor(key.getBackgroundColor()).darker().darker();
        } else if (key.isDemoPressed()) {
            return DEMO_PRESSED_COLOR;
        } else {
            return SwingUtils.fromInstrumentColor(key.getBackgroundColor());
        }
    }

    private Stroke getStroke(Border border) {
        switch (border.getType()) {
            case DOTTED:
                return new BasicStroke(border.getWidth(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[]{20.0f}, 0.0f);
            case SOLID:
                return new BasicStroke(border.getWidth());
        }
        throw new IllegalArgumentException("This border type is not supported");
    }

    private void writeKeyText(InstrumentKey instrumentKey, Graphics2D graphics, java.awt.Shape shape) {
        FontMetrics metrics = graphics.getFontMetrics();
        graphics.setColor(TEXT_COLOR);
        Rectangle bounds = shape.getBounds();
        double offset = 0;

        List<String> displays = new ArrayList<>();

        if (instrumentKey.isNameDisplayed()) {
            displays.add(instrumentKey.getName());
        }

        if (instrumentKey.isNoteAndOctaveDisplayed()) {
            displays.add(instrumentKey.getSound().getNote().toString() + instrumentKey.getSound().getOctave());
        }

        if (instrumentKey.isPersistanceDisplayed()) {
            displays.add(instrumentKey.getSound().getPersistence() + " ms");
        }

        if (instrumentKey.isVolumeDisplayed()) {
            displays.add(((int) (instrumentKey.getSound().getVolume() * 100)) + "%");
        }

        if (instrumentKey.isFrequencyDisplayed()) {
            NumberFormat formatter = new DecimalFormat("#0.000");
            displays.add(formatter.format(instrumentKey.getSound().getFrequency()) + " Hz");
        }

        final double spacing = ((bounds.getHeight() - 2 * TEXT_PADDING_Y) / displays.size()); // 5 text entries
        offset = FONT_SIZE + TEXT_PADDING_Y;
        for (String s : displays) {
            writeText(metrics, graphics, s, bounds.getCenterX(), bounds.getMinY(), offset);
            offset += spacing;
        }

    }

    private void writeText(FontMetrics metrics, Graphics2D graphics, String text, double centerX, double minY, double offsetY) {
        int textWidth = metrics.stringWidth(text);
        double posX = centerX - textWidth / 2;

        graphics.drawString(text, (int) posX, (int) (minY + offsetY));
    }

    private boolean instrumentFitsInScreen(gaudrophone.instrument.geometry.shapes.Rectangle bounds) {
        int lastWidthPx = (int) (bounds.getWidth() * lastWidth);
        int lastHeightPx = (int) (bounds.getHeight() * lastHeight);

        return lastHeightPx <= panel.getHeight() && lastWidthPx <= panel.getWidth();
    }

    private void replaceComponentsKeepAspect() {

        double xPx;
        double yPx;

        Point p;

        for (InstrumentKey key : instrument.getInstrumentKeys()) {
            p = key.getShape().getCenter();
            xPx = (p.getX() * lastWidth);
            yPx = (p.getY() * lastHeight);
            key.getShape().setPosition(new Point(xPx / panel.getWidth(), yPx / panel.getHeight()));
        }
    }

    private void resizeComponentsKeepRatio(gaudrophone.instrument.geometry.shapes.Rectangle bounds) {
        gaudrophone.instrument.geometry.Shape s;
        Point c;

        boolean handleWidth = true;

        // Both resize at the same time
        if (lastWidth > panel.getWidth() && lastHeight > panel.getHeight()) {
            double percentageLossWidth = lastWidth * 1.0 / panel.getWidth();
            double percentageLossHeight = lastHeight * 1.0 / panel.getHeight();

            // Must resize using height in this case
            if (percentageLossHeight > percentageLossWidth) {
                handleWidth = false;
            }
        }

        for (InstrumentKey key : instrument.getInstrumentKeys()) {
            s = key.getShape();
            c = s.getCenter();

            double heightPx = lastHeight * s.getHeight();
            double widthPx = lastWidth * s.getWidth();

            if (handleWidth && lastWidth > panel.getWidth()) {

                // The pixel heightWidth ratio must be preserved
                double newWidthPx = s.getWidth() * panel.getWidth();
                double heightBefore = s.getHeight();
                s.setHeight((heightPx * newWidthPx / widthPx) / panel.getHeight());
                double heightAfter = s.getHeight();

                // The Y Position must be adusted to the new bounds
                double heightLossRatio = heightAfter / heightBefore;
                s.setPosition(new Point(c.getX(), c.getY() * heightLossRatio));

            } else if (lastHeight > panel.getHeight()) {
                // The pixel heightWidth ratio must be preserved
                double newHeightPx = s.getHeight() * panel.getHeight();
                double widthBefore = s.getWidth();
                s.setWidth((widthPx * newHeightPx / heightPx) / panel.getWidth());
                double widthAfter = s.getWidth();

                // The X Position must be adusted to the new bounds
                double widthLossRatio = widthAfter / widthBefore;
                s.setPosition(new Point(c.getX() * widthLossRatio, c.getY()));
            }

        }
    }

    private void resizeComponentsKeepAspect() {

        double heightPx;
        double widthPx;

        gaudrophone.instrument.geometry.Shape s;

        for (InstrumentKey key : instrument.getInstrumentKeys()) {
            s = key.getShape();
            heightPx = (lastHeight * s.getHeight());
            widthPx = (lastWidth * s.getWidth());

            double newHeight = heightPx / panel.getHeight();
            double newWidth = widthPx / panel.getWidth();

            s.setHeight(newHeight);
            s.setWidth(newWidth);
        }
    }

    private gaudrophone.instrument.geometry.shapes.Rectangle findBestBounds(gaudrophone.instrument.geometry.shapes.Rectangle bounds) {

        gaudrophone.instrument.geometry.shapes.Rectangle result = new gaudrophone.instrument.geometry.shapes.Rectangle();
        double boundsHeight = bounds.getHeight();
        double boundsWidth = bounds.getWidth();

        double maxWidth = panel.getWidth();
        double maxHeight = panel.getHeight();

        double testWidth = boundsWidth * maxHeight / boundsHeight;
        double testHeight = boundsHeight * maxWidth / boundsWidth;

        if (testWidth < maxWidth) {
            result.setWidth(testWidth);
            result.setHeight(maxHeight);
        } else {
            result.setWidth(maxWidth);
            result.setHeight(testHeight);
        }

        return result;
    }

    private void resizeComponentsKeepRatioBigger(gaudrophone.instrument.geometry.shapes.Rectangle bounds) {

        if (bounds.getWidth() > 1 || bounds.getHeight() > 1) {
            double scale = 0;
            if (bounds.getWidth() > 1) {
                scale = 1 / bounds.getWidth();
            } else {
                scale = 1 / bounds.getHeight();
            }

            bounds.setWidth(bounds.getWidth() * scale);
            bounds.setHeight(bounds.getHeight() * scale);
        }

        gaudrophone.instrument.geometry.shapes.Rectangle maxBounds = findBestBounds(bounds);

        //widthToUse = (int) maxBounds.getWidth();
        //heightToUse = (int) maxBounds.getHeight();
//        gaudrophone.instrument.geometry.Shape s;
//        Point c;
//
//        boolean handleWidth = true;
//
//        // Both resize at the same time
//        if (lastWidth < panel.getWidth() && lastHeight < panel.getHeight()) {
//            double percentageWinWidth = 1.0 * panel.getWidth() / lastWidth;
//            double percentageWinHeight = panel.getHeight() * 1.0 / lastHeight;
//
//            // Must resize using height in this case
//            if (percentageWinHeight > percentageWinWidth) {
//                handleWidth = false;
//            }
//        }
//        for (InstrumentKey key : instrument.getInstrumentKeys()) {
//            s = key.getShape();
//            c = s.getCenter();
//
//            double heightPx = lastHeight * s.getHeight();
//            double widthPx = lastWidth * s.getWidth();
//
//            if (handleWidth && lastWidth < panel.getWidth()) {
//
//                // The pixel heightWidth ratio must be preserved
//                double newWidthPx = s.getWidth() * panel.getWidth();
//                double heightBefore = s.getHeight();
//                s.setHeight((heightPx * newWidthPx / widthPx) / panel.getHeight());
//                double heightAfter = s.getHeight();
//
//                // The Y Position must be adusted to the new bounds
//                double heightLossRatio = heightAfter / heightBefore;
//                s.setPosition(new Point(c.getX(), c.getY() * heightLossRatio));
//
//            } else if (lastHeight < panel.getHeight()) {
//                // The pixel heightWidth ratio must be preserved
//                double newHeightPx = s.getHeight() * panel.getHeight();
//                double widthBefore = s.getWidth();
//                s.setWidth((widthPx * newHeightPx / heightPx) / panel.getWidth());
//                double widthAfter = s.getWidth();
//
//                // The X Position must be adusted to the new bounds
//                double widthLossRatio = widthAfter / widthBefore;
//                s.setPosition(new Point(c.getX() * widthLossRatio, c.getY()));
//            }
//
//        }
    }

    public void setProportionalResize(boolean selected) {
        this.autisticResize = selected;
        this.lastHeight = 0;
        this.lastWidth = 0;
    }

    public boolean isProportionalResize() {
        return autisticResize;
    }

}
