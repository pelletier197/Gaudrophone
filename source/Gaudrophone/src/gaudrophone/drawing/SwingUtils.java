/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.drawing;

import gaudrophone.instrument.apparence.Color;
import gaudrophone.instrument.geometry.Ellipse;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;
import gaudrophone.instrument.geometry.Shape;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.util.Arrays;

/**
 *
 * @author sunny
 */
public class SwingUtils {

    public static Point pointToRelative(java.awt.Point p, int width, int height) {
        return new Point(((double) p.x) / width, ((double) p.y) / height);
    }

    public static java.awt.Point relativeToPoint(Point p, int width, int height) {
        return new java.awt.Point((int) (p.getX() * width), (int) (p.getY() * height));
    }

    public static int[][] relativeToPointsXY(Point[] points, int width, int height) {

        int[] x = new int[points.length];
        int[] y = new int[points.length];

        Point p;
        for (int i = 0; i < points.length; i++) {
            p = points[i];
            x[i] = (int) (p.getX() * width);
            y[i] = (int) (p.getY() * height);
        }

        return new int[][]{x, y};
    }

    public static java.awt.Shape generateShapeFromRelative(Shape shape, int width, int height) {
        if (shape instanceof Polygon) {
            return generatePolygonFromRelative((Polygon) shape, width, height);
        } else {
            return generateEllipseFromRelative((Ellipse) shape, width, height);
        }
    }

    public static java.awt.Color fromInstrumentColor(Color color) {
        return new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public static Color toInstrumentColor(java.awt.Color color) {
        return new Color(color.getAlpha(), color.getRed(), color.getGreen(), color.getBlue());
    }

    private static java.awt.Shape generatePolygonFromRelative(Polygon polygon, int width, int height) {
        
        int[][] xy = relativeToPointsXY(polygon.getPoints(), width, height);

        // Note that rotation is not supported
        return new java.awt.Polygon(xy[0], xy[1], xy[0].length);
    }

    private static java.awt.Shape generateEllipseFromRelative(Ellipse ellipse, int width, int height) {

        java.awt.Point center = relativeToPoint(ellipse.getCenter(), width, height);
        double eWidth = (width * ellipse.getWidth());
        double eHeight = (height * ellipse.getHeight());

        java.awt.geom.Ellipse2D ell = new Ellipse2D.Double(center.x - eWidth / 2, center.y - eHeight / 2, eWidth, eHeight);
        // Note that rotation is not supported
        return ell;
    }

}
