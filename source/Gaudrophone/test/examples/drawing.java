/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

import gaudrophone.drawing.SwingUtils;
import gaudrophone.instrument.Note;
import gaudrophone.instrument.sound.Sound;
import gaudrophone.instrument.geometry.Ellipse;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Shape;
import gaudrophone.instrument.geometry.shapes.Octogon;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.sound.generated.GeneratedSoundException;
import gaudrophone.instrument.sound.generated.GeneratedSoundInstrument;
import gaudrophone.instrument.sound.generated.GeneratedSoundManager;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author sunny
 */
public class drawing {

    public static void main(String[] args) throws GeneratedSoundException {

        Shape octo = new Octogon();

        Shape ellipse = new Ellipse(0.2, 0.3, 0.2,0.2);
//
        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                ((Graphics2D) g).draw(SwingUtils.generateShapeFromRelative(ellipse, getWidth(), getHeight()));

                java.awt.Shape sh = SwingUtils.generateShapeFromRelative(octo, getWidth(), getHeight());
                ((Graphics2D) g).draw(sh);
            }

        };

        JSlider rotate = new JSlider(0, 360);
        rotate.addChangeListener((e) -> {
            octo.setAngle(rotate.getValue());
            ellipse.setAngle(rotate.getValue());
            panel.repaint();
        });
        JSlider height = new JSlider(1, 100);
        height.addChangeListener((e) -> {
            double val = height.getValue() * 1.0 / 100;
            octo.setHeight(val);
            ellipse.setHeight(val);
            panel.repaint();
        });
        JSlider width = new JSlider(1, 100);
        width.addChangeListener((e) -> {
            double val = width.getValue() * 1.0 / 100;
            octo.setWidth(val);
            ellipse.setWidth(val);
            panel.repaint();
        });
        JSlider posX = new JSlider(0, 100);
        posX.addChangeListener((e) -> {
            double val = posX.getValue() * 1.0 / 100;
            octo.setPosition(new Point(val, octo.getCenter().getY()));
            ellipse.setPosition(new Point(val, ellipse.getCenter().getY()));
            panel.repaint();
        });
        JSlider posY = new JSlider(0, 100);
        posY.addChangeListener((e) -> {
            double val = posY.getValue() * 1.0 / 100;
            octo.setPosition(new Point(octo.getCenter().getX(), val));
            ellipse.setPosition(new Point(ellipse.getCenter().getX(), val));
            panel.repaint();
        });

        JLabel el = new JLabel("E : ");
        JLabel rec = new JLabel("R : ");

        Sound s1 = new GeneratedSound(Note.A, 3, GeneratedSoundInstrument.VIOLIN);
        Sound s2 = new GeneratedSound(Note.C, 5, GeneratedSoundInstrument.ACOUSTIC_GUITAR);

        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                Point p2 = SwingUtils.pointToRelative(p, panel.getWidth(), panel.getHeight());
                if (ellipse.isInside(p2)) {
                    el.setText("E : IN");
                    s1.play();

                } else {
                    el.setText("E : OUT");
                }

                if (octo.isInside(p2)) {
                    rec.setText("R : IN");
                    s2.play();

                } else {
                    rec.setText("R : OUT");
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                s1.stop();
                s2.stop();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        panel.add(rotate);
        panel.add(height);
        panel.add(width);
        panel.add(posX);
        panel.add(posY);
        panel.add(el);
        panel.add(rec);

        JFrame frame = new JFrame();
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.show();
    }

    private static int[][] getXY(Point[] points) {

        int[] x = new int[points.length];
        int[] y = new int[points.length];

        for (int i = 0; i < points.length; i++) {
            x[i] = (int) points[i].getX();
            y[i] = (int) points[i].getY();
        }

        return new int[][]{x, y};
    }
}

