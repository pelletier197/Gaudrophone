/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound;

import gaudrophone.instrument.Note;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loup
 */
public class SoundTest {
    
    protected Sound sound1;
    
    public SoundTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        sound1 = new SoundImpl();
        sound1.setNote(Note.A);
        sound1.setOctave(4);
    }
    
    @After
    public void tearDown() {
        sound1.stop();
    }

    /**
     * Test of getNote method, of class Sound.
     */
    @Test
    public void testGetNote() {
        // Arrange
        System.out.println("getNote");
        
        // Act
        Note expResult = Note.A;
        Note result = sound1.getNote();
        
        // Assert
        assertEquals(expResult, result);
    }

    /**
     * Test of setNote method, of class Sound.
     */
    @Test
    public void testSetNote() {
        // Arrange
        System.out.println("setNote");
        
        // Act
        Note pNote = Note.E;
        sound1.setNote(pNote);
        Note result = sound1.getNote();
        
        // Assert
        assertEquals(pNote, result);
    }

    /**
     * Test of getOctave method, of class Sound.
     */
    @Test
    public void testGetOctave() {
        // Arrange
        System.out.println("getOctave");
        
        // Act
        int expResult = 4;
        int result = sound1.getOctave();
        
        // Assert
        assertEquals(expResult, result);
    }

    /**
     * Test of setOctave method, of class Sound.
     */
    @Test
    public void testSetOctave() {
        // Arrange
        System.out.println("setOctave");
        
        // Act
        int pOctave = 5;
        sound1.setOctave(pOctave);
        int result = sound1.getOctave();
        
        // Assert
        assertEquals(pOctave, result);
    }

    /**
     * Test of getPersistence method, of class Sound.
     */
    @Test
    public void testGetPersistence() {
        // Arrange
        System.out.println("getPersistence");
        
        // Act
        long expResult = 50;
        long result = sound1.getPersistence();
        
        // Assert
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setPersistence method, of class Sound.
     */
    @Test
    public void testSetPersistence() {
        // Arrange
        System.out.println("setPersistence");
        
        // Act
        long pPersistence = 0;
        sound1.setPersistence(pPersistence);
        long result = sound1.getPersistence();
        
        // Assert
        assertEquals(pPersistence, result, 0.0);
    }

    /**
     * Test of getVolume method, of class Sound.
     */
    @Test
    public void testGetVolume() {
        // Arrange
        System.out.println("getVolume");
        
        // Act
        double expResult = 1.0;
        double result = sound1.getVolume();
        
        // Assert
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setVolume method, of class Sound.
     */
    @Test
    public void testSetVolume() {
        // Arrange
        System.out.println("setVolume");
        
        // Act
        double pVolume = 0.0;
        sound1.setVolume(pVolume);
        double result = sound1.getVolume();
        
        // Assert
        assertEquals(pVolume, result, 0.0);
    }

    /**
     * Test of getFrequency method, of class Sound.
     */
    @Test
    public void testGetFrequency() {
        // Arrange
        System.out.println("getFrequency");
        
        // Act
        double expResult = 440.0;
        double result = sound1.getFrequency();
        
        // Assert
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setFrequency method, of class Sound.
     */
    @Test
    public void testSetFrequency() {
        // Arrange
        System.out.println("setFrequency");
        
        // Act
        Note noteExpected = Note.A;
        int octaveExpected = 0;
        double frequency = 55.0000;
        sound1.setFrequency(frequency);
        Note noteResult = sound1.getNote();
        int octaveResult = sound1.getOctave();
        
        // Assert
        assertEquals("The note is wrong", noteExpected, noteResult);
        assertEquals("The octave is wrong", octaveExpected, octaveResult);
    }

    /**
     * Test of getMidiNote method, of class Sound.
     */
    @Test
    public void testGetMidiNote() {
        System.out.println("getMidiNote");
        int expResult = 69;
        int result = sound1.getMidiNote();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMidiNote method, of class Sound.
     */
    @Test
    public void testSetMidiNote() {
        System.out.println("setMidiNote");
        int midiNote = 57;
        sound1.setMidiNote(midiNote);
        int result = sound1.getMidiNote();
        assertEquals(midiNote, result);
    }

    /**
     * Test of computeMidiNoteFromNoteOctave method, of class Sound.
     */
    @Test
    public void testComputeMidiNoteFromNoteOctave() {
        System.out.println("computeMidiNoteFromNoteOctave");
        Note note = Note.A;
        int octave = 4;
        int expResult = 69;
        int result = Sound.computeMidiNoteFromNoteOctave(note, octave);
        assertEquals(expResult, result);
    }

    /**
     * Test of computeFrequencyFromNoteOctave method, of class Sound.
     */
    @Test
    public void testComputeFrequencyFromNoteOctave() {
        System.out.println("computeFrequencyFromNoteOctave");
        Note note = Note.A;
        int octave = 4;
        double expResult = 440.0;
        double result = Sound.computeFrequencyFromNoteOctave(note, octave);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of computeMidiNoteFromFrequency method, of class Sound.
     */
    @Test
    public void testComputeMidiNoteFromFrequency() {
        System.out.println("computeMidiNoteFromFrequency");
        double frequency = 440.0;
        int expResult = 57;
        int result = Sound.computeMidiNoteFromFrequency(frequency);
        assertEquals(expResult, result);
    }

    public class SoundImpl extends Sound {

        public void playSound() {
        }

        public void stopSound() {
        }
    }
    
}
