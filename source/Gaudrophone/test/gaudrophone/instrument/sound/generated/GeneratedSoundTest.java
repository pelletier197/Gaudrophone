/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound.generated;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loup
 */
public class GeneratedSoundTest {
    
    protected GeneratedSound instance;
    
    public GeneratedSoundTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new GeneratedSound();
        instance.setPersistence(1000);
    }
    
    @After
    public void tearDown() {
        instance.stop();
    }

    /**
     * Test of getInstrument method, of class GeneratedSound.
     */
    @Test
    public void testGetInstrument() {
        System.out.println("getInstrument");
        GeneratedSoundInstrument expResult = GeneratedSoundInstrument.PIANO;
        GeneratedSoundInstrument result = instance.getInstrument();
        assertEquals(expResult, result);
    }

    /**
     * Test of playSound method, of class GeneratedSound.
     */
    @Test
    public void testPlaySound() {
        System.out.println("playSound");
        instance.playSound();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(GeneratedSoundTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of stopSound method, of class GeneratedSound.
     */
    @Test
    public void testStopSound() {
        System.out.println("stopSound");
        instance.stopSound();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
