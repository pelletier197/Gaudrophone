/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Note;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loup
 */
public class FrameInfoTest {
    
    protected FrameInfo frameInfo1;
    
    public FrameInfoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        frameInfo1 = new FrameInfo(Note.A, 4);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNote method, of class FrameInfo.
     */
    @Test
    public void testGetNote() {
        System.out.println("getNote");
        Note expResult = Note.A;
        Note result = frameInfo1.getNote();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOctave method, of class FrameInfo.
     */
    @Test
    public void testGetOctave() {
        System.out.println("getOctave");
        int expResult = 4;
        int result = frameInfo1.getOctave();
        assertEquals(expResult, result);
    }
    
}
