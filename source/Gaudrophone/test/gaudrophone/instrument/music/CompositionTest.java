/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Note;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loup
 */
public class CompositionTest {
    
    protected static Composition composition1;
    protected static Composition composition2;
    
    public CompositionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        List<MusicalEvent> eventList = new ArrayList<>();
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.D, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.D, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.D_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.D_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.E, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.E, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.F, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.F, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.F_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.F_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.G, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.G, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.G_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.G_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.A, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.A, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.A_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.A_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.B, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.B, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C, 5)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C, 5)));
        composition1 = new Composition(eventList, "");
        
        eventList.clear();
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 5000L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C_SHARP, 4)));
        composition2 = new Composition(eventList, "");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDuration method, of class Composition.
     */
    @Test
    public void testGetDuration() {
        System.out.println("getDuration");
        long expResult = 6500;
        long result = composition1.getDuration();
        assertEquals(expResult, result);
    }

    /**
     * Test of EventAtFrame method, of class Composition.
     */
    @Test
    public void testEventAtFrame() {
        System.out.println("EventAtFrame");
        int frame = 4;
        MusicalEvent expResult = new MusicalEvent(MusicalEventType.PAUSE, 500L);
        MusicalEvent result = composition1.eventAtFrame(frame);
        assertEquals(expResult.type, result.type);
        assertEquals(expResult.data, result.data);
    }

    /**
     * Test of NumberOfFrames method, of class Composition.
     */
    @Test
    public void testNumberOfFrames() {
        System.out.println("NumberOfFrames");
        int expResult = 39;
        int result = composition1.numberOfFrames();
        assertEquals(expResult, result);
    }

    /**
     * Test of NumberOfFrames method, of class Composition.
     */
    @Test
    public void testNumberOfFrames_breakDownLongPause() {
        System.out.println("NumberOfFrames_breakDownLongPause");
        int expResult = 16;
        int result = composition2.numberOfFrames();
        assertEquals(expResult, result);
    }

    /**
     * Test of seekFrame method, of class Composition.
     */
    @Test
    public void testSeekFrame() {
        System.out.println("seekFrame");
        long playedTime = 3250;
        int expResult = 19;
        int result = composition1.seekFrame(playedTime);
        assertEquals(expResult, result);
    }
    
}
