/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.template.PianoTemplateStrategy;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loup
 */
public class CompositionRepositoryTest {
    
    static List<String>stringList1;
    static List<String>stringList2;
    
    public CompositionRepositoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        stringList1 = new ArrayList<>();
        stringList1.add("// Pour mettre du silence, on peut utiliser la \"note\" \"X\" (avec la durée en dessous, en option)\n");
        stringList1.add("60\n");
        stringList1.add("\n");
        stringList1.add("C D X F G A C D5\n");
        
        stringList2 = new ArrayList<>();
        stringList2.add("//  Take  My    Hand     Let's   Walk  Down  Town    \n");
        stringList2.add("60\n");
        stringList2.add("  | e4 e4 e4 e4 e4 e4 e4 e4 | g4 g4 g4 g4 g4 g4 g4 g4 |\n");
        stringList2.add("  | c3 c3 c3 c3 c3 c3 c3 c3 | b3 b3 b3 b3 b3 b3 b3 b3 |\n");
        stringList2.add("  | g3 g3 g3 g3 g3 g3 g3 g3 | g3 g3 g3 g3 g3 g3 g3 g3 |\n");
        stringList2.add("    _  _  _  _  _  _  _  _    _  _  _  _  _  _  _  _ ");

    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of parseFile method, of class CompositionRepository.
     * @throws java.lang.Exception
     */
    @Test
    public void testParseFile() throws Exception {
        System.out.println("parseFile");
        String filePath = "C:\\Users\\Loup Labelle\\Sources\\GLO-2004_Lossless\\Documentation utile\\pieces_musicales\\SuperMarioBros_Overworld-MainTheme.txt";
        CompositionRepository instance = new CompositionRepository();
        Composition result = instance.parseFile(filePath);
        Instrument instrument = new PianoTemplateStrategy().generateInstrument();
        MusicBox musicBox = new MusicBox(instrument);
        musicBox.setComposition(result);
        
        musicBox.start();
    }

    /**
     * Test of parseFile method, of class CompositionRepository.
     * @throws java.lang.Exception
     */
    @Test
    public void testParseFile_furElise() throws Exception {
        System.out.println("parseFile");
        String filePath = "C:\\Users\\Loup Labelle\\Sources\\GLO-2004_Lossless\\Documentation utile\\pieces_musicales\\fur_elise.txt";
        CompositionRepository instance = new CompositionRepository();
        Composition result = instance.parseFile(filePath);
        Instrument instrument = new PianoTemplateStrategy().generateInstrument();
        MusicBox musicBox = new MusicBox(instrument);
        musicBox.setComposition(result);
        
        musicBox.start();
    }

    /**
     * Test of parseStringList method, of class CompositionRepository.
     * @throws java.lang.Exception
     */
    @Test
    public void parseStringList_NotesSimples() throws Exception {
        System.out.println("parseFile");
        CompositionRepository instance = new CompositionRepository();
        Composition result = instance.parseStringList(stringList1, "");
        Instrument instrument = new PianoTemplateStrategy().generateInstrument();
        MusicBox musicBox = new MusicBox(instrument);
        musicBox.setComposition(result);
        
        musicBox.start();
    }

    /**
     * Test of parseStringList method, of class CompositionRepository.
     * @throws java.lang.Exception
     */
    @Test
    public void parseStringList_Accords() throws Exception {
        System.out.println("parseFile");
        CompositionRepository instance = new CompositionRepository();
        Composition result = instance.parseStringList(stringList2, "");
        Instrument instrument = new PianoTemplateStrategy().generateInstrument();
        MusicBox musicBox = new MusicBox(instrument);
        musicBox.setComposition(result);
        
        musicBox.start();
    }
    
}
