/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.sound.generated.GeneratedSoundManager;
import gaudrophone.instrument.template.PianoTemplateStrategy;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loup
 */
public class MusicBoxIT {
    
    public MusicBoxIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setComposition method, of class MusicBox.
     */
    @Test
    public void testSetComposition() {
        System.out.println("setComposition");
        Composition composition = null;
        MusicBox instance = null;
        instance.setComposition(composition);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMusicEventListener method, of class MusicBox.
     */
    @Test
    public void testSetMusicEventListener() {
        System.out.println("setMusicEventListener");
        MusicalEventListener eventListener = null;
        MusicBox instance = null;
        instance.setMusicEventListener(eventListener);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isPlaying method, of class MusicBox.
     */
    @Test
    public void testIsPlaying() {
        System.out.println("isPlaying");
        MusicBox instance = null;
        boolean expResult = false;
        boolean result = instance.isPlaying();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of start method, of class MusicBox.
     * @throws java.lang.Exception
     */
    @Test
    public void testStart() throws Exception {
        System.out.println("start");
        GeneratedSoundManager.getInstance();
        CompositionRepository rep = new CompositionRepository();
        Composition composition1 = rep.parseFile("C:\\Users\\Loup Labelle\\Sources\\GLO-2004_Lossless\\Documentation utile\\pieces_musicales\\SuperMarioBros_Overworld-MainTheme.txt");
        Instrument instr = new PianoTemplateStrategy().generateInstrument();
        MusicBox musicBox = new MusicBox(instr);
        musicBox.setComposition(composition1);
        musicBox.start();
    }

    /**
     * Test of startLoop method, of class MusicBox.
     */
    @Test
    public void testStartLoop() {
        System.out.println("startLoop");
        MusicBox instance = null;
        instance.startLoop();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of startAt method, of class MusicBox.
     */
    @Test
    public void testStartAt() {
        System.out.println("startAt");
        double fraction = 0.0;
        MusicBox instance = null;
        instance.startAt(fraction);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of resume method, of class MusicBox.
     */
    @Test
    public void testResume() {
        System.out.println("resume");
        MusicBox instance = null;
        instance.resume();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of stop method, of class MusicBox.
     */
    @Test
    public void testStop() {
        System.out.println("stop");
        MusicBox instance = null;
        instance.stop();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
