/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.gaudrohero;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.music.Composition;
import gaudrophone.instrument.music.MusicBox;
import gaudrophone.instrument.music.MusicalEvent;
import gaudrophone.instrument.music.MusicalEventListener;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author sunny
 */
public class GaudroHeroTest {

    private static MusicBoxMock mock;
    private static InstrumentKey key;
    private GaudroHero hero;

    @BeforeClass
    public static void setUp() {
        mock = new MusicBoxMock();
        key = new InstrumentKey();
    }

    @Before
    public void beforeTest() {
        hero = new GaudroHero();
        hero.setMusicBox(mock);
    }

    @Test
    public void testUserClickAfter() throws InterruptedException {
        mock.simulatePressEvent(key);
        Thread.sleep(180);
        hero.pressKey(key); // The user clicked just on time, so he should have points
        Assert.assertTrue(hero.getPoints() == 1);
    }

    @Test
    public void testUserClickTooLate() throws InterruptedException {
        mock.simulatePressEvent(key);
        Thread.sleep(220);
        hero.pressKey(key); // The user clicked just on time, so he should have points
        Assert.assertTrue(hero.getPoints() == 0);
    }

    @Test
    public void testUserClickExact() throws InterruptedException {
        mock.simulatePressEvent(key);
        hero.pressKey(key); // The user clicked just on time, so he should have points
        Assert.assertTrue(hero.getPoints() == 1);
    }

    @Test
    public void testUserClickExactInverse() throws InterruptedException {
        hero.pressKey(key); // The user clicked just on time, so he should have points
        mock.simulatePressEvent(key);

        Assert.assertTrue(hero.getPoints() == 1);
    }

    @Test
    public void testUserDestroyStreak() throws InterruptedException {
        for (int i = 0; i < 5; i++) {
            hero.pressKey(key); // The user clicked just on time, so he should have points
            mock.simulatePressEvent(key);
        }

        Assert.assertTrue(hero.getScoreMultiplier() == 2);
        Assert.assertTrue(hero.getPoints() == 5);

        hero.pressKey(key); // The user clicked just on time, so he should have points
        mock.simulatePressEvent(key);
        Assert.assertTrue(hero.getPoints() == 7);

        // press too soon
        Thread.sleep(220);
        hero.pressKey(key);
        Thread.sleep(220);
        mock.simulatePressEvent(key);
        Assert.assertTrue(hero.getPoints() == 7);
        Assert.assertTrue(hero.getScoreMultiplier() == 1);

    }

    @Test
    public void testUserClickButNoEvent() throws InterruptedException {

        for (int i = 0; i < 5; i++) {
            hero.pressKey(key); // The user clicked just on time, so he should have points
            mock.simulatePressEvent(key);
        }
        Assert.assertTrue(hero.getScoreMultiplier() == 2);
        Thread.sleep(220);
        hero.pressKey(key); // The user clicked just on time, so he should have points
        Thread.sleep(220);

        // He failed
        Assert.assertTrue(hero.getScoreMultiplier() == 1);
    }

    @Test
    public void testNoClickingToEvent() throws InterruptedException {

        for (int i = 0; i < 5; i++) {
            hero.pressKey(key); // The user clicked just on time, so he should have points
            mock.simulatePressEvent(key);
        }
        Assert.assertTrue(hero.getScoreMultiplier() == 2);

        // Wait to simulate the next event so it isn't in the same sequence.
        Thread.sleep(20);

        System.out.println("Before test");
        mock.simulatePressEvent(key);
        Thread.sleep(220);

        // He failed
        Assert.assertTrue(hero.getScoreMultiplier() == 1);
    }

    private static class MusicBoxMock extends MusicBox {

        private MusicalEventListener listener;

        public MusicBoxMock() {
            super(new Instrument());
        }

        @Override
        public void setMusicEventListener(MusicalEventListener eventListener) {

            this.listener = eventListener;
        }

        public void simulatePressEvent(InstrumentKey key) {
            listener.onKeyPressed(key);
        }

        @Override
        public boolean isPlaying() {
            return true;
        }

        @Override
        public void stop() {
        }

    }
}
